package com.wxs.gulimall.cart.interceptor;

import com.wxs.common.constant.AuthServiceConstant;
import com.wxs.common.constant.CartConstant;
import com.wxs.common.vo.MemberEntityVo;
import com.wxs.gulimall.cart.to.UserInfoTo;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author:Adolph
 * @create:2022/6/17 在执行目标方法之前，判断用户的登录状态，并封装传递给controller目标请求
 */
@Component
public class CartInterceptor implements HandlerInterceptor {
    /**
     * threadLocal:每一个请求，都会创建一个线程，每一个线程中threadLocal独立，但是同一个请求中的内容后面可以共享
     */
    public static ThreadLocal<UserInfoTo> threadLocal = new ThreadLocal<>();

    /**
     * 目标方法执行之前
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        UserInfoTo userInfoTo = new UserInfoTo();
        MemberEntityVo memberEntityVo = (MemberEntityVo) request.getSession().getAttribute(AuthServiceConstant.LOGIN_USER_SESSION_KEY);
        if (memberEntityVo != null) {
            //用户登录了
            userInfoTo.setUserId(memberEntityVo.getId());
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                //user-key
                String cookieName = cookie.getName();
                if (CartConstant.TEMP_USER_COOKIE_NAME.equals(cookieName)){
                    userInfoTo.setUserKey(cookie.getValue());
                    userInfoTo.setIsNotUserKey(true);
                }
            }
        }
        //如果没有临时用户一定分配一个临时用户
        if (StringUtils.isEmpty(userInfoTo.getUserKey())){
            String userKey = UUID.randomUUID().toString().replace("-", "");
            userInfoTo.setUserKey(userKey);
        }
        //目标方法执行之前,将封装结果存入threadLocal
        threadLocal.set(userInfoTo);
        return true;
    }

    /**
     * 业务执行之后执行,分配临时用户，让浏览器保存
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
        UserInfoTo userInfoTo = threadLocal.get();
        if (!userInfoTo.getIsNotUserKey()){
            Cookie cookie = new Cookie(CartConstant.TEMP_USER_COOKIE_NAME,userInfoTo.getUserKey());
            cookie.setMaxAge(CartConstant.TEMP_USER_COOKIE_TIMEOUT);
            cookie.setDomain(CartConstant.TEMP_USER_COOKIE_SCOPE);
            response.addCookie(cookie);
        }
    }
}
