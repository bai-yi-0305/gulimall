package com.wxs.gulimall.cart.service;

import com.wxs.gulimall.cart.vo.Cart;
import com.wxs.gulimall.cart.vo.CartItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
public interface CartService {
    CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    CartItem getCartItem(Long skuId);

    Cart getCart() throws ExecutionException, InterruptedException;

    void clearCart(String cartKey);

    void checkedItem(Long skuId, Integer check);

    void countItem(Long skuId, Integer num);

    void deleteItem(Long skuId);

    List<CartItem> getCheckedCartItems(Long userKey);

    List<CartItem> getCheckedCartItems();
}
