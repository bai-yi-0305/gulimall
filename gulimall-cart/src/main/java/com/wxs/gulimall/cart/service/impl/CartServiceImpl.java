package com.wxs.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.wxs.common.constant.AuthServiceConstant;
import com.wxs.common.utils.R;
import com.wxs.gulimall.cart.feign.ProductFeignService;
import com.wxs.gulimall.cart.interceptor.CartInterceptor;
import com.wxs.gulimall.cart.service.CartService;
import com.wxs.gulimall.cart.to.SkuInfoEntityTo;
import com.wxs.gulimall.cart.to.UserInfoTo;
import com.wxs.gulimall.cart.vo.Cart;
import com.wxs.gulimall.cart.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
@Service
public class CartServiceImpl implements CartService {

    public static final String CART_PREFIX = "gulimall:cart:";

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private ThreadPoolExecutor executor;

    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String res = (String) cartOps.get(skuId.toString());
        if (StringUtils.isEmpty(res)) {
            //远程查询sku信息
            CartItem cartItem = new CartItem();
            CompletableFuture<Void> skuInfo = CompletableFuture.runAsync(() -> {
                R r = productFeignService.info(skuId);
                SkuInfoEntityTo skuInfoEntity = r.getData("skuInfo", new TypeReference<SkuInfoEntityTo>() {
                });
                if (skuInfoEntity != null) {
                    cartItem.setChecked(true);
                    cartItem.setTitle(skuInfoEntity.getSkuTitle());
                    cartItem.setCount(num);
                    cartItem.setDefaultImage(skuInfoEntity.getSkuDefaultImg());
                    cartItem.setPrice(skuInfoEntity.getPrice());
                    cartItem.setSkuId(skuId);
                }
            }, executor);
            //远程查询skuSaleAttr
            CompletableFuture<Void> skuSaleAttrValue = CompletableFuture.runAsync(() -> {
                List<String> skuSaleAttrValues = productFeignService.getSkuSaleAttrValues(skuId);
                cartItem.setSkuAttr(skuSaleAttrValues);
            }, executor);
            CompletableFuture.allOf(skuInfo, skuSaleAttrValue).get();
            cartOps.put(skuId.toString(), JSON.toJSONString(cartItem));
            return cartItem;
        } else {
            //购物车由此商品，数量增加
            CartItem cartItem = JSON.parseObject(res, CartItem.class);
            cartItem.setCount(cartItem.getCount() + num);
            cartOps.put(cartItem.getSkuId().toString(), JSON.toJSONString(cartItem));
            return cartItem;
        }
    }

    @Override
    public CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String res = (String) cartOps.get(skuId.toString());
        return JSON.parseObject(res, CartItem.class);
    }

    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {
        Cart cart = new Cart();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null) {
            //登录
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            //判断临时购物车中有没有未合并的数据
            String tempCartKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> tempCartItems = getCartItems(tempCartKey);
            if (!CollectionUtils.isEmpty(tempCartItems)) {
                //临时购物车中有数据，合并数据
                for (CartItem tempCartItem : tempCartItems) {
                    addToCart(tempCartItem.getSkuId(), tempCartItem.getCount());
                }
                //清空临时购物车数据
                clearCart(tempCartKey);
            }
            //合并之后获取登录购物车数据(包含临时购物车数据)
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        } else {
            //未登录，获取临时购物车信息
            String cartKey = CART_PREFIX + userInfoTo.getUserKey();
            //获取购物车中所有购物项
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        }
        return cart;
    }

    @Override
    public void clearCart(String cartKey) {
        stringRedisTemplate.delete(cartKey);
    }

    @Override
    public void checkedItem(Long skuId, Integer check) {
        CartItem cartItem = getCartItem(skuId);
        cartItem.setChecked(check == 1);
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.put(skuId.toString(), JSON.toJSONString(cartItem));
    }

    @Override
    public void countItem(Long skuId, Integer num) {
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCount(num);
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.put(skuId.toString(), JSON.toJSONString(cartItem));
    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    /**
     * 传用户id查询购物项
     * @param userKey
     * @return
     */
    @Override
    public List<CartItem> getCheckedCartItems(Long userKey) {
        String cartKey = CART_PREFIX + userKey;
        return getCheckedCartItems(cartKey);
    }

    /**
     * 不传用户id查询购物项
     * @return
     */
    @Override
    public List<CartItem> getCheckedCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo == null) {
            return null;
        } else {
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            return getCheckedCartItems(cartKey);
        }
    }

    private List<CartItem> getCheckedCartItems(String cartKey) {
        List<CartItem> cartItems = getCartItems(cartKey);
        if (!CollectionUtils.isEmpty(cartItems)) {
            //获取所有被选中的购物项
            List<CartItem> checkedCartItems = cartItems.stream().filter(CartItem::getChecked).collect(Collectors.toList());
            //远程查询 更新所有被选中的购物项的价格
            String checkedCartItemIdJson = JSON.toJSONString(cartItems.stream().map(CartItem::getSkuId).collect(Collectors.toList()));
            R r = productFeignService.getSkuPrice(checkedCartItemIdJson);
            if (r.getCode() != 0) {
                //TODO 自定义异常。。。
                throw new RuntimeException("价格查询失败");
            }
            Map<Long, BigDecimal> decimalMap = r.getData(new TypeReference<Map<Long, BigDecimal>>() {
            });
            return checkedCartItems.stream().map(cartItem -> {
                cartItem.setPrice(decimalMap.get(cartItem.getSkuId()));
                return cartItem;
            }).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * 获取购物车中的所有购物项
     *
     * @param cartKey
     * @return
     */
    private List<CartItem> getCartItems(String cartKey) {
        BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(cartKey);
        //拿到临时购物车中的所有数据
        List<Object> values = ops.values();
        if (!CollectionUtils.isEmpty(values)) {
            return values.stream().map(obj -> {
                String jsonCartItem = (String) obj;
                return JSON.parseObject(jsonCartItem, CartItem.class);
            }).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    /**
     * 获取操作的购物车
     *
     * @return
     */
    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (userInfoTo.getUserId() != null) {
            cartKey = CART_PREFIX + userInfoTo.getUserId();
        } else {
            cartKey = CART_PREFIX + userInfoTo.getUserKey();
        }
        return stringRedisTemplate.boundHashOps(cartKey);
    }
}
