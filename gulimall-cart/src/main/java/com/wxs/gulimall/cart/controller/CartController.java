package com.wxs.gulimall.cart.controller;

import com.wxs.common.utils.R;
import com.wxs.gulimall.cart.interceptor.CartInterceptor;
import com.wxs.gulimall.cart.service.CartService;
import com.wxs.gulimall.cart.to.UserInfoTo;
import com.wxs.gulimall.cart.vo.Cart;
import com.wxs.gulimall.cart.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/checkedCartItems/{userKey}")
    @ResponseBody
    public R getCheckedCartItems(@PathVariable("userKey") Long userKey) {
        List<CartItem> data = cartService.getCheckedCartItems(userKey);
        return data != null ? R.ok().put("data", data) : R.error("查询购物项失败");
    }

    @GetMapping("/checkedCartItems")
    @ResponseBody
    public R getCheckedCartItems() {
        List<CartItem> data = cartService.getCheckedCartItems();
        return data != null ? R.ok().put("data", data) : R.error("查询购物项失败");
    }

    /**
     * 浏览器有一个cookie：user-key;用来标识用户身份，一个月之后过期
     * 如果第一次使用jd的购物车功能，都会给一个临时的用户身份
     * 浏览器保存以后，每一访问都会带上这个cookie
     * <p>
     * 登录：session有
     * 没登录：按照cookie里面带的user-key创建一个临时用户
     * 第一次：如果没有临时用户，帮忙创建一个临时用户
     *
     * @return
     */
    @GetMapping("/cart.html")
    public String cartListPage(Model model) throws ExecutionException, InterruptedException {
        Cart cart = cartService.getCart();
        model.addAttribute("cart", cart);
        return "cartList";
    }

    @GetMapping("/checkedItem")
    public String checkedItem(@RequestParam("skuId") Long skuId,
                              @RequestParam("check") Integer check) {
        cartService.checkedItem(skuId, check);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num) {
        cartService.countItem(skuId, num);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId) {
        cartService.deleteItem(skuId);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * RedirectAttributes：
     * 1、addFlashAttribute():将数据放到session中，可以在页面中取出，但是只能去一次
     * 2、addAttribute():将数据放在转发路径的后面，拼接上去。http://cart.gulimall.com/addToCartListSuccess?skuId=?
     *
     * @param skuId
     * @param num
     * @param attributes
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes attributes) throws ExecutionException, InterruptedException {
        cartService.addToCart(skuId, num);
        attributes.addAttribute("skuId", skuId);
        return "redirect:http://cart.gulimall.com/addToCartListSuccess";
    }

    @GetMapping("/addToCartListSuccess")
    public String addToCartListSuccess(@RequestParam("skuId") Long skuId, Model model) {
        CartItem cartItem = cartService.getCartItem(skuId);
        model.addAttribute("cartItem", cartItem);
        return "success";
    }
}
