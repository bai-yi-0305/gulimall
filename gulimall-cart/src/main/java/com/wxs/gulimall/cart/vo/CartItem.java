package com.wxs.gulimall.cart.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/16
 * 购物车中的每一项商品
 */
@Data
public class CartItem {
    /**
     * 商品id
     */
    private Long skuId;
    /**
     * 该商品是否被选中、默认true选中
     */
    private Boolean checked = true;
    /**
     * 商品标题
     */
    private String title;
    /**
     * 商品默认图片
     */
    private String defaultImage;
    /**
     * 商品sku信息(套餐信息)
     */
    private List<String> skuAttr;
    /**
     * 商品价格
     */
    private BigDecimal price;
    /**
     * 商品数量
     */
    private Integer count;
    /**
     * 商品小计价格
     */
    private BigDecimal totalPrice;

    /**
     * 计算当前购物项总价
     * @return
     */
    public BigDecimal getTotalPrice() {
        return this.price.multiply(new BigDecimal("" + this.count));
    }
}
