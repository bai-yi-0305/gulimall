package com.wxs.gulimall.cart.feign;

import com.wxs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {

    @GetMapping("/product/skuinfo/info/{skuId}")
     R info(@PathVariable("skuId") Long skuId);

    @GetMapping("/product/skusaleattrvalue/cart/{skuId}")
    List<String> getSkuSaleAttrValues(@PathVariable("skuId") Long skuId);

    @PostMapping("/product/skuinfo/newSkuPrice")
    R getSkuPrice(@RequestBody String jsonId);
}
