package com.wxs.gulimall.cart.to;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
@Data
public class UserInfoTo {
    /**
     * 登录用户id
     */
    private Long userId;
    /**
     * 临时用户userKey
     */
    private String userKey;
    /**
     * 是否拥有userKey
     */
    private Boolean isNotUserKey = false;
}
