package com.wxs.gulimall.cart.vo;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/16
 * 整个购物车
 */
public class Cart {

    /**
     * 购物车中所有商品
     */
    private List<CartItem> items;
    /**
     * 有几件商品
     */
    private Integer countNum;
    /**
     * 有几种商品
     */
    private Integer countType;
    /**
     * 商品总价
     */
    private BigDecimal totalAmount;
    /**
     * 商品减免价格
     */
    private BigDecimal reduce = new BigDecimal("0.00");


    public Integer getCountNum() {
        int count = 0;
        if (!CollectionUtils.isEmpty(this.items)){
            for (CartItem item : items) {
                count += item.getCount();
            }
        }
        return count;
    }

    public Integer getCountType() {
        return this.items.size();
    }

    public BigDecimal getTotalAmount() {
        BigDecimal amount = new BigDecimal("0");
        //1、计算购物项总价
        if (!CollectionUtils.isEmpty(this.items)){
            for (CartItem item : items) {
                if (item.getChecked()){
                    BigDecimal totalPrice = item.getTotalPrice();
                    amount = amount.add(totalPrice);
                }
            }
        }
        //2、减去优惠总价
        return amount.subtract(reduce);
    }

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public void setCountNum(Integer countNum) {
        this.countNum = countNum;
    }

    public void setCountType(Integer countType) {
        this.countType = countType;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
