package com.wxs.common.constant;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
public class AuthServiceConstant {
    /**
     * 短信验证码key的前缀
     */
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code";
    /**
     * 同一个手机号码60秒之内只能发送一次验证码
     */
    public static final long SMS_INTERVAL_TIME = 60;
    /**
     * 用户登录成功的key
     */
    public static final String LOGIN_USER_SESSION_KEY = "loginUser";
}
