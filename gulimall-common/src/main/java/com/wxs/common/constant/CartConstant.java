package com.wxs.common.constant;

/**
 * @author:Adolph
 * @create:2022/6/17
 */
public class CartConstant {
    /**
     * 临时用户的user-key
     */
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    /**
     * 临时userKey过期时间：一个月
     */
    public static final int TEMP_USER_COOKIE_TIMEOUT = 30 * 24 * 60 * 60;
    /**
     * 临时用户的userKey的作用域
     */
    public static final String TEMP_USER_COOKIE_SCOPE = "gulimall.com";
}
