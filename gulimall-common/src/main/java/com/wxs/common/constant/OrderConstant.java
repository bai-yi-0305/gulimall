package com.wxs.common.constant;

/**
 * @author:Adolph
 * @create:2022/6/20
 */
public class OrderConstant {
    /**
     * 用户订单token
     */
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token";

    public enum OrderConstantEnum{
    //订单状态【0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单】
        CREATE_NEW(0,"待付款"),
        PAYED(1,"已付款"),
        SENDED(2,"已发货"),
        FINISHED(3,"已完成"),
        CANCELLED(4,"已取消"),
        SERVICING(5,"售后中"),
        SERVICED(6,"售后完成");

        private int code;
        private String msg;

        OrderConstantEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
