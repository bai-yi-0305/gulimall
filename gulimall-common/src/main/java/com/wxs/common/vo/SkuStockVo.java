package com.wxs.common.vo;

import lombok.Data;

/**
 * @author:Adolph
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
