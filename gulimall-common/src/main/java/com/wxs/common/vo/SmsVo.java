package com.wxs.common.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@Data
public class SmsVo {
    private String phone;
    private String code;
}
