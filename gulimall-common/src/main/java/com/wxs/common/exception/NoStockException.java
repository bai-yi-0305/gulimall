package com.wxs.common.exception;

/**
 * @author:Adolph
 * @create:2022/6/21
 */
public class NoStockException extends RuntimeException{

    private Long skuId;

    public NoStockException(Long skuId) {
        super("商品id:" + skuId + "库存不足");
    }

    public NoStockException(String message) {
        super(message);
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
}
