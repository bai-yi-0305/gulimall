package com.wxs.common.to;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/5/25
 */
@Data
public class SkuHasStockTo {
    private Long skuId;
    private Boolean hasStock;
}
