package com.wxs.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/23
 */
@Data
public class StockLockedTo {
    /**
     * 库存工作单id
     */
    private Long id;
    /**
     * 工作单详情
     */
    private StockDetailTo detailTo;
}
