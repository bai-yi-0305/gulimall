package com.wxs.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author:Adolph
 */
@Data
public class SpuBoundsTo {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
