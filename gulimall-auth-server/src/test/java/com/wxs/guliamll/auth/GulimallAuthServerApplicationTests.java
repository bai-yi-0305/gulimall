package com.wxs.guliamll.auth;


import cn.hutool.core.util.RandomUtil;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GulimallAuthServerApplicationTests {

	@Test
	public void contextLoads() {
		String code = RandomUtil.randomNumbers(6);
		System.out.println("code = " + code);
	}

}
