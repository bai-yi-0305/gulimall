package com.wxs.guliamll.auth.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @author:Adolph
 * @create:2022/6/5
 */
@ControllerAdvice(basePackages = "com.wxs.guliamll.auth.controller.LoginController")
public class AuthServerException {

}
