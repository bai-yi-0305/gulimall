package com.wxs.guliamll.auth.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/8
 */
@Data
public class SocialUser {
    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private long created_at;
}
