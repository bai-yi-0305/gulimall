package com.wxs.guliamll.auth.feign;

import com.wxs.common.utils.R;
import com.wxs.guliamll.auth.vo.LoginVo;
import com.wxs.guliamll.auth.vo.OauthGiteeLoginVo;
import com.wxs.guliamll.auth.vo.RegisterVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author:Adolph
 * @create:2022/6/6
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {

    @PostMapping("/member/member/register")
    R register(@RequestBody RegisterVo vo);

    @PostMapping("/member/member/login")
    R login(@RequestBody LoginVo vo);

    @PostMapping("/member/member/oauth2/login")
    R oauth2LoginAndRegister(@RequestBody OauthGiteeLoginVo vo);
}
