package com.wxs.guliamll.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.wxs.common.utils.R;
import com.wxs.common.vo.MemberEntityVo;
import com.wxs.guliamll.auth.feign.MemberFeignService;
import com.wxs.guliamll.auth.utils.HttpUtils;
import com.wxs.guliamll.auth.vo.GiteeRoot;
import com.wxs.guliamll.auth.vo.OauthGiteeLoginVo;
import com.wxs.guliamll.auth.vo.SocialUser;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static com.wxs.common.constant.AuthServiceConstant.LOGIN_USER_SESSION_KEY;

/**
 * @author:Adolph
 * @create:2022/6/8
 */
@Controller
public class Oauth2Controller {

    @Autowired
    private MemberFeignService memberFeignService;

    @GetMapping("/oauth2/gitee/success")
    public String gitee(@RequestParam("code") String code, HttpSession session) throws Exception {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> querys = new HashMap<>();
        Map<String, String> body = new HashMap<>();
        body.put("grant_type", "authorization_code");
        body.put("code", code);
        body.put("client_id", "e96cc815f33ac59f516fef28f61c0c12047f288534c1800eeb826153cf775102");
        body.put("redirect_uri", "http://auth.gulimall.com/oauth2/gitee/success");
        body.put("client_secret", "ecc1c172b9c57983ce9ad51ff2ee71df0e333eddd67d1272d62c027c150f7fe7");
        HttpResponse response = HttpUtils.doPost("https://gitee.com", "/oauth/token", "post",
                headers, querys, body);
        if (response.getStatusLine().getStatusCode() != 200) {
            //获取accessToken失败，重新登录
            return "redirect:http://auth.gulimall.com/login.html";
        }
        //获取accessToken
        String json = EntityUtils.toString(response.getEntity());
        SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
        String accessToken = socialUser.getAccess_token();
        GiteeRoot root =  getGiteeId(accessToken);
        if (root == null){
            return "redirect:http://auth.gulimall.com/login.html";
        }else {
            String id = root.getId();
            OauthGiteeLoginVo oauthGiteeLoginVo = new OauthGiteeLoginVo();
            oauthGiteeLoginVo.setName(root.getName());
            oauthGiteeLoginVo.setGiteeId(id);
            oauthGiteeLoginVo.setAccessToken(accessToken);
            //知道当前是哪个社交用户
            //1）、当前用户如果是第一次进网站，自动注册进来（为当前社交用户生成会员信息账号，以后这个社交帐号就对应指定的会员）
            R r = memberFeignService.oauth2LoginAndRegister(oauthGiteeLoginVo);
            if (r.getCode() == 0){
                MemberEntityVo memberEntityVo = r.getData(new TypeReference<MemberEntityVo>() {
                });
//                System.out.println("memberEntity = " + memberEntity);
                //1、第一次使用session：命令浏览器保存jseesionId这个cookie，以后浏览器访问哪个网站就会带上这个网站的cookie
                //子域之间：gulimall.com(父域名)、 auth.gulimall.com(子域名)、 order.gulimall.com(子域名)、
                //浏览器在保存jsessionId的时候（指定域名为父域名），即使是子域系统，也能让父域直接使用
                //TODO 1、默认发的令牌，session=sasda 作用域：当前域；（解决子域session共享问题）
                //TODO 2、使用json的序列化方式来序列化对象数据存到redis中
                session.setAttribute(LOGIN_USER_SESSION_KEY,memberEntityVo);
//                new Cookie("JSESSIONID","").setDomain(".gulimall.com")
//                servletResponse.addCookie();
                return "redirect:http://gulimall.com";
            }else {
                return "redirect:http://auth.gulimall.com/login.html";
            }
        }
    }

    private GiteeRoot getGiteeId(String accessToken) throws Exception {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> querys = new HashMap<>();
        querys.put("access_token",accessToken);
        HttpResponse response = HttpUtils.doGet("https://gitee.com",
                "/api/v5/user", "get", headers, querys);
        String json = EntityUtils.toString(response.getEntity());
        return JSON.parseObject(json,GiteeRoot.class);
    }
}
