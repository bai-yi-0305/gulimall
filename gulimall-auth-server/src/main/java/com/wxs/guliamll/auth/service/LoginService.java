package com.wxs.guliamll.auth.service;

import com.wxs.common.utils.R;
import com.wxs.guliamll.auth.vo.LoginVo;
import com.wxs.guliamll.auth.vo.RegisterVo;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
public interface LoginService {
    R sendCode(String phone);

    R verifyCodeAndtransferReg(RegisterVo registerVo);

    R login(LoginVo loginVo);
}
