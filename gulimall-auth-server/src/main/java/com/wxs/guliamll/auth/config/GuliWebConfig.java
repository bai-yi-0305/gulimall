package com.wxs.guliamll.auth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@Configuration
public class GuliWebConfig implements WebMvcConfigurer {
    /**
     *     @GetMapping("/login.html")
     *     public String login(){
     *         return "login";
     *     }
     *     @GetMapping("/reg.html")
     *     public String reg(){
     *         return "reg";
     *     }
     */
    /**
     * 试图映射，要是直接跳转页面的空方法，则可以使用addViewControllers方法实现
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/reg.html").setViewName("reg");
    }
}
