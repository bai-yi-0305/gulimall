package com.wxs.guliamll.auth.controller;

import com.alibaba.fastjson.TypeReference;
import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.utils.R;
import com.wxs.common.vo.MemberEntityVo;
import com.wxs.guliamll.auth.service.LoginService;
import com.wxs.guliamll.auth.vo.LoginVo;
import com.wxs.guliamll.auth.vo.RegisterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.wxs.common.constant.AuthServiceConstant.LOGIN_USER_SESSION_KEY;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

//    使用viewController在GuliWebConfig配置中跳转页面
    @GetMapping("/login.html")
    public String login(HttpSession session){
        if (session.getAttribute(LOGIN_USER_SESSION_KEY) == null){
            return "login";
        }else {
            return "redirect:http://gulimall.com";
        }
    }
//
//    @GetMapping("/reg.html")
//    public String reg(){
//        return "reg";
//    }

    @GetMapping("/sms/code")
    @ResponseBody
    public R sendCode(@RequestParam("phone") String phone) {
        return loginService.sendCode(phone);
    }

    /**
     * RedirectAttributes:模拟重定向携带数据
     * 原理：就是利用session携带数据，只要跳到下一个页面，取出数据之后，session中的数据就会删掉
     *
     * @param registerVo
     * @param bindingResult
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/register")
    public String register(@Validated RegisterVo registerVo, BindingResult bindingResult,
                           RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = bindingResult.getFieldErrors()
                    .stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
            redirectAttributes.addFlashAttribute("errors", errorMap);
            //Request method 'POST' not support;错误
            //用户注册-->/register[POST]-->forward:/reg.html(路径映射默认是GET方式访问，所以出错)
            //校验出错，转发注册页
            return "redirect:http://auth.gulimall.com/reg.html";
        }
        //校验验证码
        //注册成功回到登录页面,调用远程服务注册
        R r = loginService.verifyCodeAndtransferReg(registerVo);
        if (r.getCode() != 0){
            if (r.getCode() == BizCodeEnum.USERNAME_EXIST_EXCEPTION.getCode()){
                Map<String, String> errors = new HashMap<>();
                errors.put("msg", (String) r.get("msg"));
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }else if (r.getCode() == BizCodeEnum.PHONE_EXIST_EXCEPTION.getCode()){
                Map<String, String> errors = new HashMap<>();
                errors.put("msg", (String) r.get("msg"));
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }else {
                Map<String, String> errors = new HashMap<>();
                errors.put("code", (String) r.get("msg"));
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }
        }
        //login.html前面加 "/" 表示从当前项目域名路径为准（/login.html）
        return "redirect:http://auth.gulimall.com/login.html";
    }

    @PostMapping("/login")
    public String login(LoginVo loginVo, RedirectAttributes redirectAttributes, HttpSession session){
        R r = loginService.login(loginVo);
        if (r.getCode() != 0){
            Map<String, String> errors = new HashMap<>();
            errors.put("msg", (String) r.get("msg"));
            redirectAttributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/login.html";
        }
        //TODO 登录成功
        MemberEntityVo memberEntityVo = r.getData(new TypeReference<MemberEntityVo>() {});
        session.setAttribute(LOGIN_USER_SESSION_KEY,memberEntityVo);
        return "redirect:http://gulimall.com";
    }
}
