package com.wxs.guliamll.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * SpringSession的核心原理：
 * 	1、@EnableRedisHttpSession注解给容器中导入了RedisHttpSessionConfiguration配置
 * 		1)、给容器中添加了一个组件：RedisOperationsSessionRepository（属于SessionRepository）：redis操作session。session的增删改查封装类
 *		2)、SessionRepositoryFilter（servlet.filter）：session存储过滤器，每一个请求过来都必须经过filter
 *			1、创建该类的时候，自动从容器中获取SessionRepository（容器中有的例如自己放进去的：RedisOperationsSessionRepository）
 *			2、原始的request，response都被包装了。这个方法中实现：SessionRepositoryFilter.doFilterInternal(HttpServletRequest request,HttpServletResponse response, FilterChain filterChain)
 *				request被包装成了：SessionRepositoryRequestWrapper wrappedRequest
 *				response被包装成了：SessionRepositoryResponseWrapper wrappedResponse
 *			3、以后获取session，使用request.getSession();但是因为request已经被包装成了SessionRepositoryRequestWrapper，
 *				所以获取session就变成了：wrappedRequest.getSession();
 *				（session的获取方法就发生了改变，SessionRepositoryRequestWrapper中getSession()方法进行了重写！！使用的都是自己的逻辑）
 *			4、在SessionRepositoryRequestWrapper中getSession()方法获取的session就是-->SessionRepository（也就是）--->RedisOperationsSessionRepository
 *	装饰者模式！！！
 *
 */
@EnableRedisHttpSession //整合redis作为session存储
@EnableFeignClients(basePackages = "com.wxs.guliamll.auth.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallAuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GulimallAuthServerApplication.class, args);
	}

}
