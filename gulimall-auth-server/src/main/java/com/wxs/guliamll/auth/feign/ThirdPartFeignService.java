package com.wxs.guliamll.auth.feign;

import com.wxs.common.utils.R;
import com.wxs.common.vo.SmsVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@FeignClient("gulimall-third-part")
public interface ThirdPartFeignService {
    
    @PostMapping("/sms/sendCode")
    R sendCode(@RequestBody SmsVo smsVo);
}
