package com.wxs.guliamll.auth.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.utils.R;
import com.wxs.common.vo.SmsVo;
import com.wxs.guliamll.auth.feign.MemberFeignService;
import com.wxs.guliamll.auth.feign.ThirdPartFeignService;
import com.wxs.guliamll.auth.service.LoginService;
import com.wxs.guliamll.auth.vo.LoginVo;
import com.wxs.guliamll.auth.vo.RegisterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

import static com.wxs.common.constant.AuthServiceConstant.SMS_CODE_CACHE_PREFIX;
import static com.wxs.common.constant.AuthServiceConstant.SMS_INTERVAL_TIME;
import static com.wxs.common.exception.BizCodeEnum.SMS_CODE_EXCEPTION;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private ThirdPartFeignService thirdPartFeignService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MemberFeignService memberFeignService;

    @Override
    public R sendCode(String phone) {
        //TODO 1、接口防刷（前端暴露了请求验证码的接口，可能被恶意请求）

        //2、验证码请求频率问题：防止前端刷新页面，重复请求发送验证码（60s内只能发送一次）
        String redisCode = stringRedisTemplate.opsForValue().get(SMS_CODE_CACHE_PREFIX + phone);
        if (!StringUtils.isEmpty(redisCode)) {
            String[] redisCodeStrings = redisCode.split("_");
            DateTime oldSmsValueTime = DateUtil.parse(redisCodeStrings[1]);
            DateTime nowTime = DateUtil.date();
            long diffTime = DateUtil.between(oldSmsValueTime, nowTime, DateUnit.SECOND);
            if (diffTime < SMS_INTERVAL_TIME) {
                //没有等够60s，不能在发
                return R.error(SMS_CODE_EXCEPTION.getCode(), SMS_CODE_EXCEPTION.getMsg());
            }
        }
        SmsVo smsVo = new SmsVo();
        smsVo.setPhone(phone);
        String code = RandomUtil.randomNumbers(6);
        smsVo.setCode(code);
        //远程调用第三方服务发送验证码
        R r = thirdPartFeignService.sendCode(smsVo);
        if (r.getCode() == 0) {
            //发送验证码成功，在保存验证码到redis中
            String codeAndTime = code + "_" + DateUtil.date();
            stringRedisTemplate.opsForValue().set(SMS_CODE_CACHE_PREFIX + phone, codeAndTime, 5, TimeUnit.MINUTES);
            return R.ok();
        }
        return R.error(BizCodeEnum.SEND_SMS_CODE_ERROR.getCode(), BizCodeEnum.SEND_SMS_CODE_ERROR.getMsg());
    }

    @Override
    public R verifyCodeAndtransferReg(RegisterVo registerVo) {
        String code = registerVo.getCode();
        String redisCode = stringRedisTemplate.opsForValue().get(SMS_CODE_CACHE_PREFIX + registerVo.getPhone());
        if (!StringUtils.isEmpty(redisCode)) {
            if (code.equals(redisCode.split("_")[0])) {
                //删除验证码，令牌机制
                stringRedisTemplate.delete(SMS_CODE_CACHE_PREFIX + registerVo.getPhone());
                //远程调用注册
                return memberFeignService.register(registerVo);
            }else {
                return R.error("验证码错误");
            }
        }
        return R.error("验证码错误");
    }

    @Override
    public R login(LoginVo loginVo) {
        return memberFeignService.login(loginVo);
    }
}
