package com.wxs.guliamll.auth.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/7
 */
@Data
public class LoginVo {
    private String loginacct;
    private String password;
}
