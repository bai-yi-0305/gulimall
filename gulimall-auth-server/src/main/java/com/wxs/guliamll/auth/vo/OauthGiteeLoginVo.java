package com.wxs.guliamll.auth.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/8
 */
@Data
public class OauthGiteeLoginVo {
    private String name;
    private String giteeId;
    private String accessToken;
}
