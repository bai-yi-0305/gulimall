package com.wxs.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.wxs.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000121605898";
    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCDT/4DYdnJ4Y6i+RXVmJwYqraJsqf4gYc2euzsJ0CJ3Z5cUbaiXjF7CLiAN1bT+mtXbKDCqu2MXp0lPJqnGXtrdxA1cVjTubaIbgGAJ7LxbwbkY0MN6Q5OC9RNIe70+jXx9a1zU9Gybn5NQjVAD012cI4HVw7VS26i1p7jGHzW300ZWXm1a2LHvleTlFMNGcUOBWgXr02ARYgVPFxtS/MLQQhzxrLkhca42muRsOloQ9H8CNsUcy3beYQSJic+YvsEvRxZ474eNJX+l0EhhEgiXsBIhS0MARCrLV3CNLIj/dTlpgh8oOEF35HdpkvHYv6lTn400qiQtfibmap33rpnAgMBAAECggEASsBy3gIKNT7OMECUCXBDDFfCakjmDjXOf1kjHpyJxPIAgJWT3oK4sE2amoLmTu4zxST1r1wWtt31n4kMkSuUcuzosUPBSWZjXu8gs4/l8kM+LS3EslqF5jAdv3sdyR/XLdKp1wlUSrtT8y9JmRodAtu4Pr10mD/Ez7ZV5Pm/t6G+OvHSAV/OorsX4+MUwsJ23/hd5eO5Z/4YRFGVT5l8SOwnj78K0aYlThnzyBnQQfSRqHRRAQNLVDJ+dsejo1mfNXrS5G6W/7TcBtx9W55A+7biWCcl4tWVSIMBx6he/udMTuxT/Ul5XaIg9wq/8J4KyOrRquDiaFjSdMYpfrEzIQKBgQC7L3OFYQFewueVJ+aOMRFW8KDLPRRsmCMjVR5YNMHi7gUSURD5tz7j9kZpNvaqIm0x23af3hnVQGTBG6EH3o0PumkvxjjVVQ/EBkH6xq7+/c/SDFhIEb2y+Gu6qZ+ZniMXRm8xoDtAo87+/67a6nWP/wOfr5RaIRdpYPPcFIiRvQKBgQCzli8YPHe9Phv9oZeWUul8ZenpMlCqiSukOCGSzQlyOK2sAFsFLGRYA0a7f8UyNaOMhd2CSppdve0xIJUGfJd1jEwES9sHNXqYMhQWEw+3d48+77mFF0dKR1qAk50JiqtfTFG+uUgvVY1ivnyRFyEekNhehcZacw45D7dNTEo08wKBgED7HRBdd5tM8BYQjvInyIw05q0HXDLCsdgPMYSPdAtyc6JDMDBwU83RbeoSoZi4cM3RESI4eSFVxGRhUgqoULH4FZH3Lz2n1NxJC9ih5pgwxld136PNPNyHbuaTAgvTIP1wfyNsK4WqGC7f1fS7VeqSLdDepBKR0E3N0XpCSpLFAoGARS7DH8QToLyvjcrVmdyBBCJCtUpj1v6W6GGTVXlVfUImbY+JgPm0Ak/ozMyRDCGryrO7PLk+tIQ5Pua9Yk1XD7qngjQywZkxjDYmlMZtFJGjhmxVfNcV6KCKVeQH6CgaktDU4POBwnJj0DlvxXkykhLct7UzT57pSMQsKkQMhk8CgYEAug7DJEETmzPRdKCsdp+02Kkq0MOTUEcKpgm38xorUnXzjSEaddZSIK6mscyQus/4AfUv+C3+JMipYDTmjoWDXj+R+4WXjz5KnAEJT/rAl5twlWyd1D46nA9k+Y9/8CIwx+eNKcupUr0i62Ze3U9QEENwWsQATKSmD9oltlb/5kA=";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAschUFd5thMFdtynp/sw4hPWSdUKROOtoyo/853zb6qRpXRObYzn7YbHRL1yoGpFyGIcYocTm4lqRQoj3hjqHD+OhcLjSxaeDa47YJ9brMRTqmhA91qksNBPQCXWv3hAM0S3qGuktoaxk7j5qviJjRvkbUzxv5D0w8wDfJ+9uNoeuR3o7NZXjB+qMOF5fYNoVPzVH5Wyl+Bt6x7uMlRsf9D3Xi86kdtEkQ2wO+Yw/a5mkcvRNplJD5eslUfZk0fLhjPK2uKNfKCAyXxH+A40pLPAsM72mI25NbaByJlfHJ5EsQF81ZZhl57+KOehpJ1uLLaI6Ol+YpniEuxfyOKQDMwIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url = "http://kpy9ik.natappfree.cc/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

//    private String timeout = "30";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
//                + "\"timeout_express\":\""+ timeout +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
