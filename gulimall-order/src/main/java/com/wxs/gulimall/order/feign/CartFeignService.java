package com.wxs.gulimall.order.feign;

import com.wxs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author:Adolph
 * @create:2022/6/19
 */
@FeignClient("gulimall-cart")
public interface CartFeignService {

    @GetMapping("/checkedCartItems/{userKey}")
    R getCheckedCartItems(@PathVariable("userKey") Long userKey);

    @GetMapping("/checkedCartItems")
    R getCheckedCartItems();
}
