package com.wxs.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author:Adolph
 * @create:2022/6/20
 */
@Data
public class FareVo {

    private OrderConfirmVo.MemberAddressVo address;

    private BigDecimal fare;

}
