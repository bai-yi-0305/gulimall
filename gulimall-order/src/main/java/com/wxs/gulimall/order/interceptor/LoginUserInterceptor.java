package com.wxs.gulimall.order.interceptor;

import com.wxs.common.constant.AuthServiceConstant;
import com.wxs.common.vo.MemberEntityVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author:Adolph
 * @create:2022/6/19
 */
@Component
public class LoginUserInterceptor implements HandlerInterceptor {

    public static ThreadLocal<MemberEntityVo> loginUser = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        if (new AntPathMatcher().match("/order/order/status/**", uri) || new AntPathMatcher().match("/payed/notify", uri)) {
            return true;
        }
        MemberEntityVo memberEntityVo = (MemberEntityVo) request.getSession().getAttribute(AuthServiceConstant.LOGIN_USER_SESSION_KEY);
        if (memberEntityVo != null){
            //登录，放行
            loginUser.set(memberEntityVo);
            return true;
        }else {
            //未登录，跳转登录页
            request.getSession().setAttribute("msg","请先登录！");
            response.sendRedirect("http://auth.gulimall.com/login.html");
            return false;
        }
    }
}
