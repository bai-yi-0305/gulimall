package com.wxs.gulimall.order;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 使用RabbitMQ
 *  1、引入amqp依赖:RabbitAutoConfiguration就会自动生效
 *  2、给容器中自动配置了：RabbitTemplate、AmqpAdmin、CachingConnectionFactory、RabbitMessagingTemplate
 *  3、给配置文件中配置相关信息即可（spring.rabbitmq.xx）
 *  4、监听消息：@RabbitListener;必须要有@EnableRabbit注解
 *      @RabbitListener: 标注在 类 + 方法 上（类上：监听那些队列）
 *      @RabbitHandler: 标注在 方法 上（重载区分不同的消息）
 *
 * 本地事务失效问题：
 *  - 同一个对象内事务方法互调默认失效，原因：绕过了代理对象，事务使用代理对象来控制
 *  - 解决：
 *      - 引入aop-starter（spring-boot-starter-aop）,引入aspectj
 *      - @EnableAspectJAutoProxy:开启aspectJ动态代理功能，以后所有的动态代理都是aspectJ创建的（即使没有接口也可以创建动态代理）
 *          exposeProxy = true:对外暴露代理对象
 *      - 本类互相调用调用对象
 *         OrderService orderService = (OrderService)AopContext.currentProxy();
 *              orderService.a();
 *              orderService.b();
 *
 * Seata控制分布式事务
 *  - 每一个微服务创建undo_log数据库表
 *  - 安装事务协调器(TC):从 https://github.com/seata/seata/releases,下载服务器软件包，将其解压缩。
 *  - 整合
 *      - 导入依赖 spring-cloud-starter-alibaba-seata、seate-all(0.7版本)
 *      - 解压并启动seata-server
 *          - registry.conf:注册中心配置，修改registry type = "nacos"
 *          - file.conf:
 *      - 所有先还要用到分布式事务的微服务使用seata DataSourceProxy代理自己的数据源
 *      - 每个微服务，都必须导入
 *          - file.conf:修改vgroup_mapping.{微服务名（application.name）}-fescar-service-group = "default"
 *          - registry.conf
 *      - 给分布式大事务的入口标注@GlobalTransaction
 *      - 每一个远程的小事务使用@Transaction
 *
 */
//@EnableAspectJAutoProxy(exposeProxy = true)
@EnableRabbit
@EnableFeignClients(basePackages = "com.wxs.gulimall.order.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallOrderApplication.class, args);
    }

}
