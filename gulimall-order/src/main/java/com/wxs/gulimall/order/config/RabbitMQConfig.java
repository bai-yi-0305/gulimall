package com.wxs.gulimall.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author:Adolph
 * @create:2022/6/18
 */
@Configuration
public class RabbitMQConfig {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     *  1、服务器（broker）收到消息回调:
     *      1)、spring.rabbitmq.publisher.confirms=true
     *      2)、设置确认回调 ConfirmCallback
     *  2、消息正确到达队列回调
     *      1)、spring.rabbitmq.publisher.returns=true
     *      2)、spring.rabbitmq.template.mandatory=true
     *      3)、设置确认回调 ReturnsCallback
     *  3、消费端确认：保证每一个消息被正确消费，才可以删除消息
     *      1)、默认是自动确认的，只要收到了消息，客户端就会自动确认，服务端就会移除这个消息
     *          问题：
     *              我们收到很多消息，自动回复给服务器ack，只有一个消息处理成功，宕机了，发生消息丢失
     *          解决问题：
     *              手动确认：spring.rabbitmq.listener.direct.acknowledge-mode = manual
     *              开启手动确认模式，只要没有明确告诉MQ，消息被处理成功，没有ack，消息就一直是unacked状态，
     *              即使Consumer宕机，消息也不会丢失，消息会重新变为ready状态，下一次有新的Consumer连接进来，就发给他处理
     *      2)、如何手动确认？
     *          channel.basicAck()：签收，业务正确完成，签收
     *          channel.basicNack()：拒签，业务出错，失败，拒签
     *          channel.basicReject()：拒签 。。。
     *
     * @PostConstruct: RabbitMQConfig对象创建完成之后，调用该方法
     */
    @PostConstruct
    public void initRabbitTemplate(){
        //设置确认回调（消息到达服务器确认）
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * 1、只要消息抵达Broker，ack = true
             * @param correlationData:当前消息的唯一关联数据（这个消息的唯一id）
             * @param ack:消息是否成功
             * @param cause：失败原因
             *
             *      - 做好消息确认机制（publisher、consumer【手动ack】）
             *      - 每一个发送的消息都在数据库做好记录，定期将失败的消息再次发送
             *
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                System.out.println("消息抵达Broker");
            }
        });

        //只要消息没到达队列，就会报错
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             *  触发时机：只要消息没有投递给指定的队列，我们就会触发这个失败回调
             * @param message:投递失败消息的详细信息
             * @param replyCode:回复状态码
             * @param replyText:回复文本内容
             * @param exchange:当时这个消息发送给哪一个交换机
             * @param routingKey：当时这个消息使用的路由键
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                System.out.println("失败的内容");
                //报错了，修改数据库当前消息的状态--->错误
            }
        });
    }
}
