package com.wxs.gulimall.order.vo;

import com.wxs.gulimall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/20
 */
@Data
public class OrderSubmitRespVo {

    private OrderEntity order;
    /**
     * 状态码：0 成功
     */
    private Integer code;
}
