package com.wxs.gulimall.order.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.constant.OrderConstant;
import com.wxs.common.exception.NoStockException;
import com.wxs.common.to.SkuHasStockTo;
import com.wxs.common.to.mq.OrderTo;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;
import com.wxs.common.utils.R;
import com.wxs.common.vo.MemberEntityVo;
import com.wxs.gulimall.order.dao.OrderDao;
import com.wxs.gulimall.order.entity.OrderEntity;
import com.wxs.gulimall.order.entity.OrderItemEntity;
import com.wxs.gulimall.order.entity.PaymentInfoEntity;
import com.wxs.gulimall.order.feign.CartFeignService;
import com.wxs.gulimall.order.feign.MemberFeignService;
import com.wxs.gulimall.order.feign.ProductFeignService;
import com.wxs.gulimall.order.feign.WareFeignService;
import com.wxs.gulimall.order.interceptor.LoginUserInterceptor;
import com.wxs.gulimall.order.service.OrderItemService;
import com.wxs.gulimall.order.service.OrderService;
import com.wxs.gulimall.order.service.PaymentInfoService;
import com.wxs.gulimall.order.to.OrderCreateTo;
import com.wxs.gulimall.order.to.SpuInfoTo;
import com.wxs.gulimall.order.to.WareSkuLockTo;
import com.wxs.gulimall.order.vo.*;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    private static final DefaultRedisScript<Long> CHECK_ORDER_TOKEN;

    private final ThreadLocal<OrderSubmitVo> orderSubmitVoThreadLocal = new ThreadLocal<>();

    @Autowired
    private ProductFeignService productFeignService;

    static {
        CHECK_ORDER_TOKEN = new DefaultRedisScript<>();
        CHECK_ORDER_TOKEN.setLocation(new ClassPathResource("checkOrderToken.lua"));
        CHECK_ORDER_TOKEN.setResultType(Long.class);
    }

    @Autowired
    private MemberFeignService memberFeignService;
    @Autowired
    private CartFeignService cartFeignService;
    @Autowired
    private ThreadPoolExecutor executor;
    @Autowired
    private WareFeignService wareFeignService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private PaymentInfoService paymentInfoService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = new OrderConfirmVo();
        MemberEntityVo memberEntityVo = LoginUserInterceptor.loginUser.get();
        //获取原始请求,每一个新开的线程都要共享请求中的数据
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        CompletableFuture<Void> getAddress = CompletableFuture.runAsync(() -> {
            //将原始请求放入新的线程中，保证在后面的拦截器中可以获取原始请求中的Cookie等数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            //远程调用查询收获地址
            R r = memberFeignService.getMemberReceiveAddress(memberEntityVo.getId());
            if (r.getCode() != 0) {
                throw new RuntimeException("地址查询失败");
            }
            List<OrderConfirmVo.MemberAddressVo> memAddress = r.getData(new TypeReference<List<OrderConfirmVo.MemberAddressVo>>() {
            });
            confirmVo.setAddress(memAddress);
        }, executor);
        CompletableFuture<Void> getCartItems = CompletableFuture.runAsync(() -> {
            //将原始请求放入新的线程中，保证在后面的拦截器中可以获取原始请求中的Cookie等数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            //远程查询选中购物项
            //带用户id查询
            //R checkedCartItems = cartFeignService.getCheckedCartItems(memberEntityVo.getId());
            //不带用户id查询
            R checkedCartItems = cartFeignService.getCheckedCartItems();
            if (checkedCartItems.getCode() != 0) {
                throw new RuntimeException((String) checkedCartItems.get("msg"));
            }
            List<OrderConfirmVo.OrderItemVo> cartItems = checkedCartItems.getData(new TypeReference<List<OrderConfirmVo.OrderItemVo>>() {
            });
            confirmVo.setItems(cartItems);
        }, executor).thenRunAsync(() -> {
            List<Long> skuIds = confirmVo.getItems().stream().map(OrderConfirmVo.OrderItemVo::getSkuId).collect(Collectors.toList());
            R r = wareFeignService.getSkuHasStock(skuIds);
            Map<Long, Boolean> stocks = r.getData(new TypeReference<List<SkuHasStockTo>>() {
            })
                    .stream().collect(Collectors.toMap(
                            SkuHasStockTo::getSkuId, SkuHasStockTo::getHasStock));
            confirmVo.setStocks(stocks);
        }, executor);

        confirmVo.setIntegration(memberEntityVo.getIntegration());
        //TODO 防重令牌
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        confirmVo.setOrderToken(token);
        stringRedisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberEntityVo.getId(), token, 30, TimeUnit.MINUTES);
        CompletableFuture.allOf(getAddress, getCartItems).get();
        return confirmVo;
    }

    /*
    //事务都是使用代理对象来控制的
    //同一个对象内事务方法互调默认失效.原因：绕过了代理对象
    @Transactional
    public void a(){
        //若是调用的是其它类中的方法，本地事务会生效，若是本地方法，则事务不生效
        b();//不生效
        c();//不生效
        //bService.b();
        //cService.c();
    }
    //Propagation.REQUIRED：表示使用它调用者所开启的事务，自己所加的事务不生效，若是调用者发生异常，也会回滚
    @Transactional(propagation = Propagation.REQUIRED)
    public void b(){

    }
    //Propagation.REQUIRES_NEW：表示使用它自己的事务，若是调用者发生异常，不会回滚
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void c(){

    }
*/

    //@GlobalTransactional //分布式事务
    @Transactional
    @Override
    public OrderSubmitRespVo submitOrder(OrderSubmitVo orderSubmitVo) {
        orderSubmitVoThreadLocal.set(orderSubmitVo);
        OrderSubmitRespVo respVo = new OrderSubmitRespVo();
        MemberEntityVo memberEntityVo = LoginUserInterceptor.loginUser.get();
        respVo.setCode(0);
        String orderToken = orderSubmitVo.getOrderToken();
        //1、验证令牌【令牌的对比和删除必须是一个原子操作，这里使用lua脚本实现】
        Long res = stringRedisTemplate.execute(CHECK_ORDER_TOKEN,
                Collections.singletonList(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberEntityVo.getId()),
                orderToken);
        if (res == 0L) {
            //验证失败,状态码设为1，标识失败
            respVo.setCode(1);
            return respVo;
        }
        //验证成功
        //创建订单
        OrderCreateTo order = createOrder();
        //验价
        BigDecimal payAmount = order.getOrder().getPayAmount();
        BigDecimal payPrice = orderSubmitVo.getPayPrice();
        if (Math.abs(payAmount.subtract(payPrice).doubleValue()) < 0.01) {
            //成功,保存订单
            saveOrder(order);
            //库存锁定
            WareSkuLockTo wareSkuLockTo = new WareSkuLockTo();
            wareSkuLockTo.setOrderSn(order.getOrder().getOrderSn());
            List<OrderConfirmVo.OrderItemVo> itemVos = order.getOrderItems().stream().map(item -> {
                OrderConfirmVo.OrderItemVo orderItemVo = new OrderConfirmVo.OrderItemVo();
                orderItemVo.setSkuId(item.getSkuId());
                orderItemVo.setCount(item.getSkuQuantity());
                orderItemVo.setTitle(item.getSkuName());
                return orderItemVo;
            }).collect(Collectors.toList());
            wareSkuLockTo.setLocks(itemVos);
            //TODO 远程锁库存
            //为了保证高并发，库存服务自己回滚，可以发消息给库存服务
            //库存服务可以使用自动解锁模式 消息队列
            R r = wareFeignService.orderLockStock(wareSkuLockTo);
            if (r.getCode() == 0) {
                //锁定成功
                respVo.setOrder(order.getOrder());
                // TODO 远程增减积分，出现异常
                //int i = 10 / 0;    //订单回滚，库存锁定不会滚
                //TODO 订单创建成功，发送消息给mq
                rabbitTemplate.convertAndSend(
                        "order-event-exchange",
                        "order.create.order",
                        order.getOrder());
                return respVo;
            } else {
                //失败
                String msg = (String) r.get("msg");
                throw new NoStockException(msg);
            }
        } else {
            respVo.setCode(2);
            return respVo;
        }
    }

    @Override
    public OrderEntity getOrderStatus(String orderSn) {
        return this.getOne(new QueryWrapper<OrderEntity>().eq("order_sn", orderSn));
    }

    @Override
    public void closeOrder(OrderEntity orderEntity) {
        //查询订单最新状态
        OrderEntity entity = this.getById(orderEntity.getId());
        if (entity != null && entity.getStatus().equals(OrderConstant.OrderConstantEnum.CREATE_NEW.getCode())) {
            //未支付，关单
            OrderEntity updatedEntity = new OrderEntity();
            updatedEntity.setStatus(OrderConstant.OrderConstantEnum.CANCELLED.getCode());
            updatedEntity.setId(orderEntity.getId());
            this.updateById(updatedEntity);
            //解锁成功，发送消息给mq
            OrderTo orderTo = new OrderTo();
            BeanUtils.copyProperties(entity, orderTo);
            try {
                //TODO 保证消息一定发送出去,每一个消息都做好日志记录（给数据库保存每一个消息的详细信息）
                //TODO 定期扫描数据库讲失败的消息重新发送
                rabbitTemplate.convertAndSend("order-event-exchange", "order.release.other", orderTo);
            } catch (AmqpException e) {
                //TODO 将没发送出去的消息重试
            }
        }
    }

    @Override
    public PayVo getOrderPay(String orderSn) {
        PayVo payVo = new PayVo();
        OrderEntity order = this.getOrderStatus(orderSn);
        payVo.setTotal_amount(order.getPayAmount().setScale(2, BigDecimal.ROUND_UP).toString());
        payVo.setOut_trade_no(order.getOrderSn());
        OrderItemEntity orderItemEntity = orderItemService.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", orderSn)).get(0);
        payVo.setSubject(orderItemEntity.getSkuName());
        payVo.setBody(orderItemEntity.getSkuAttrsVals());
        return payVo;
    }

    @Override
    public PageUtils queryPageWithItem(Map<String, Object> params) {
        MemberEntityVo memberEntityVo = LoginUserInterceptor.loginUser.get();
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>().eq("member_id", memberEntityVo.getId()).orderByDesc("id")
        );
        List<OrderEntity> orders = page.getRecords().stream().peek(order -> {
            List<OrderItemEntity> itemEntityList = orderItemService.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", order.getOrderSn()));
            order.setOrderItems(itemEntityList);
        }).collect(Collectors.toList());
        page.setRecords(orders);
        return new PageUtils(page);
    }

    @Override
    public String handlePayResult(PayAsyncVo payAsyncVo) {
        //保存交易流水
        PaymentInfoEntity paymentInfoEntity = new PaymentInfoEntity();
        paymentInfoEntity.setAlipayTradeNo(payAsyncVo.getTrade_no());
        paymentInfoEntity.setOrderSn(payAsyncVo.getOut_trade_no());
        paymentInfoEntity.setPaymentStatus(payAsyncVo.getTrade_status());
        paymentInfoEntity.setCallbackTime(payAsyncVo.getNotify_time());
        paymentInfoService.save(paymentInfoEntity);
        //修改订单的状态信息
        if (payAsyncVo.getTrade_status().equals("TRADE_SUCCESS") || payAsyncVo.getTrade_status().equals("TRADE_FINISHED")) {
            //支付成功
            String orderSn = payAsyncVo.getOut_trade_no();
            this.baseMapper.updateOrderStatus(orderSn,OrderConstant.OrderConstantEnum.PAYED.getCode());
        }
        return "success";
    }


    /**
     * 保存订单数据
     *
     * @param order
     */
    private void saveOrder(OrderCreateTo order) {
        OrderEntity orderEntity = order.getOrder();
        orderEntity.setModifyTime(new Date());
        this.save(orderEntity);
        List<OrderItemEntity> orderItems = order.getOrderItems();
        orderItemService.saveBatch(orderItems);
    }

    /**
     * 创建订单
     *
     * @return
     */
    private OrderCreateTo createOrder() {
        OrderCreateTo orderCreateTo = new OrderCreateTo();
        //构建订单信息
        OrderEntity orderEntity = buildOrder();
        //构建所有订单项
        List<OrderItemEntity> orderItems = buildOrderItems(orderEntity.getOrderSn());
        //计算价格相关数据
        computePrice(orderEntity, orderItems);

        orderCreateTo.setOrder(orderEntity);
        orderCreateTo.setOrderItems(orderItems);
        return orderCreateTo;
    }

    /**
     * 计算价格
     *
     * @param orderEntity
     * @param orderItems
     */
    private void computePrice(OrderEntity orderEntity, List<OrderItemEntity> orderItems) {
        BigDecimal total = new BigDecimal("0");
        BigDecimal coupon = new BigDecimal("0");
        BigDecimal integration = new BigDecimal("0");
        BigDecimal promotion = new BigDecimal("0");
        BigDecimal gift = new BigDecimal("0");
        BigDecimal growth = new BigDecimal("0");
        //订单总额：叠加每一个订单项的金额
        for (OrderItemEntity orderItem : orderItems) {
            BigDecimal realAmount = orderItem.getRealAmount();
            total = total.add(realAmount);
            coupon = coupon.add(orderItem.getCouponAmount());
            integration = integration.add(orderItem.getIntegrationAmount());
            promotion = promotion.add(orderItem.getPromotionAmount());
            gift = gift.add(new BigDecimal(orderItem.getGiftIntegration().toString()));
            growth = growth.add(new BigDecimal(orderItem.getGiftGrowth().toString()));
        }
        //订单总额设置
        orderEntity.setTotalAmount(total);
        //应付总额：订单总额+运费
        orderEntity.setPayAmount(total.add(orderEntity.getFreightAmount()));
        orderEntity.setPromotionAmount(promotion);
        orderEntity.setCouponAmount(coupon);
        orderEntity.setIntegrationAmount(integration);
        //设置积分信息
        orderEntity.setIntegration(gift.intValue());
        orderEntity.setGrowth(growth.intValue());
        orderEntity.setDeleteStatus(0);//未删除
    }

    /**
     * 构建订单
     *
     * @return
     */
    private OrderEntity buildOrder() {
        //1、生成订单号
        MemberEntityVo memberEntityVo = LoginUserInterceptor.loginUser.get();
        String orderSn = IdWorker.getTimeId();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(orderSn);
        orderEntity.setMemberId(memberEntityVo.getId());
        OrderSubmitVo orderSubmitVo = orderSubmitVoThreadLocal.get();
        //远程调用查询运费，收货人信息
        R r = wareFeignService.getFare(orderSubmitVo.getAddrId());
        FareVo fareVo = r.getData(new TypeReference<FareVo>() {
        });
        orderEntity.setFreightAmount(fareVo.getFare());
        orderEntity.setReceiverCity(fareVo.getAddress().getCity());
        orderEntity.setReceiverDetailAddress(fareVo.getAddress().getDetailAddress());
        orderEntity.setReceiverName(fareVo.getAddress().getName());
        orderEntity.setReceiverPhone(fareVo.getAddress().getPhone());
        orderEntity.setReceiverPostCode(fareVo.getAddress().getPostCode());
        orderEntity.setReceiverProvince(fareVo.getAddress().getProvince());
        orderEntity.setReceiverRegion(fareVo.getAddress().getRegion());
        //设置订单状态
        orderEntity.setStatus(OrderConstant.OrderConstantEnum.CREATE_NEW.getCode());

        return orderEntity;
    }

    /**
     * 构建所有订单项数据
     *
     * @param orderSn 为哪一个订单创建订单项
     * @return
     */
    private List<OrderItemEntity> buildOrderItems(String orderSn) {
        R checkedCartItems = cartFeignService.getCheckedCartItems();
        if (checkedCartItems.getCode() != 0) {
            throw new RuntimeException("远程查询购物车失败");
        }
        List<OrderConfirmVo.OrderItemVo> cartItemsData = checkedCartItems.getData(
                new TypeReference<List<OrderConfirmVo.OrderItemVo>>() {
                });
        return cartItemsData.stream().map((item) -> {
            OrderItemEntity orderItemEntity = buildOrderItem(item);
            orderItemEntity.setOrderSn(orderSn);
            return orderItemEntity;
        }).collect(Collectors.toList());
    }

    /**
     * 构建单个订单项信息
     *
     * @param item 每一个购物项信息
     * @return
     */
    private OrderItemEntity buildOrderItem(OrderConfirmVo.OrderItemVo item) {
        OrderItemEntity orderItemEntity = new OrderItemEntity();
        //1、订单信息：订单号（上面完成了）
        //2、spu信息
        R r = productFeignService.getSpuInfoBySkuId(item.getSkuId());
        if (r.getCode() != 0) {
            throw new RuntimeException("查询spu信息失败");
        }
        SpuInfoTo spuInfoTo = r.getData(new TypeReference<SpuInfoTo>() {
        });
        orderItemEntity.setSpuId(spuInfoTo.getId());
        orderItemEntity.setSpuBrand(spuInfoTo.getBrandId().toString());
        orderItemEntity.setSpuName(spuInfoTo.getSpuName());
        orderItemEntity.setCategoryId(spuInfoTo.getCatelogId());
        //3、sku信息
        orderItemEntity.setSkuId(item.getSkuId());
        orderItemEntity.setSkuName(item.getTitle());
        orderItemEntity.setSkuPic(item.getDefaultImage());
        orderItemEntity.setSkuPrice(item.getPrice());
        orderItemEntity.setSkuAttrsVals(StringUtils.collectionToDelimitedString(item.getSkuAttr(), ";"));
        orderItemEntity.setSkuQuantity(item.getCount());
        //4、优惠信息（不做）
        //5、积分信息(随便搞一下/(ㄒoㄒ)/~~)
        orderItemEntity.setGiftGrowth(item.getPrice().multiply(new BigDecimal(item.getCount().toString())).intValue());
        orderItemEntity.setGiftIntegration(item.getPrice().multiply(new BigDecimal(item.getCount().toString())).intValue());
        //6、订单价格信息
        orderItemEntity.setPromotionAmount(new BigDecimal("0"));
        orderItemEntity.setCouponAmount(new BigDecimal("0"));
        orderItemEntity.setIntegrationAmount(new BigDecimal("0"));
        //当前订单项的实际金额:总额-各种优惠
        BigDecimal orig = orderItemEntity.getSkuPrice().multiply(new BigDecimal(orderItemEntity.getSkuQuantity().toString()));
        BigDecimal real = orig.subtract(orderItemEntity.getPromotionAmount()).subtract(orderItemEntity.getCouponAmount()).subtract(orderItemEntity.getIntegrationAmount());
        orderItemEntity.setRealAmount(real);
        return orderItemEntity;
    }
}