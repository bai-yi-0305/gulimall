package com.wxs.gulimall.order.config;

import com.rabbitmq.client.Channel;
import com.wxs.gulimall.order.entity.OrderEntity;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/6/23
 */
@Configuration
public class MyRabbitMQConfig {


    /**
     * 延时队列
     * @return
     */
    @Bean
    public Queue orderDelayQueue(){
        Map<String, Object> map = new HashMap<>();
        map.put("x-dead-letter-exchange","order-event-exchange");
        map.put("x-dead-letter-routing-key","order.release.order");
        map.put("x-message-ttl",60000);
        return new Queue("order.delay.queue", true, false, false,map);
    }

    /**
     * 处理死信的队列
     * @return
     */
    @Bean
    public Queue orderReleaseQueue(){
        return new Queue("order.release.order.queue", true, false, false);
    }

    /**
     * order交换机
     * @return
     */
    @Bean
    public Exchange orderEventExchange(){
        return new TopicExchange("order-event-exchange",true,false);
    }

    /**
     * 将交换机和延迟队列绑定
     * @return
     */
    @Bean
    public Binding orderCreateOrderBinding(){
        return new Binding(
                "order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
                null);
    }

    /**
     * 将交换机和处理死的信队列绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOrderBinding(){
        return new Binding(
                "order.release.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);
    }

    /**
     * 订单释放直接和库存解锁绑定，订单解锁成功时，直接给库存服务发送消息，
     *      告诉库存服务订单以解锁成功，达到精准解锁库存。防止因给网络，延迟等原因导致订单超过30分钟未成功解锁，
     *      但是此时由于时间到了，库存查看订单状态任为新建状态，就不在进行解锁库存，消息就会确认（应该可以加入消息队列）。导致最后该库存不会被解锁。
     * @return
     */
    @Bean
    public Binding orderReleaseOtherBinding(){
        return new Binding(
                "stock.release.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);
    }
}
