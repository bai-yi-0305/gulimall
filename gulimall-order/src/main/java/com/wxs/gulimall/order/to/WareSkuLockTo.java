package com.wxs.gulimall.order.to;

import com.wxs.gulimall.order.vo.OrderConfirmVo;
import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/21
 */
@Data
public class WareSkuLockTo {

    /**
     * 订单号
     */
    private String orderSn;
    /**
     * 需要锁定的库存
     */
    private List<OrderConfirmVo.OrderItemVo> locks;
}
