package com.wxs.gulimall.order.to;

import com.wxs.gulimall.order.entity.OrderEntity;
import com.wxs.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/20
 */
@Data
public class OrderCreateTo {

    private OrderEntity order;

    private List<OrderItemEntity> orderItems;

    private BigDecimal payPrice;

    private BigDecimal fare;
}
