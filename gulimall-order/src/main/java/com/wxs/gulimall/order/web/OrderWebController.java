package com.wxs.gulimall.order.web;

import com.wxs.common.exception.NoStockException;
import com.wxs.gulimall.order.service.OrderService;
import com.wxs.gulimall.order.vo.OrderConfirmVo;
import com.wxs.gulimall.order.vo.OrderSubmitRespVo;
import com.wxs.gulimall.order.vo.OrderSubmitVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

/**
 * @author:Adolph
 * @create:2022/6/19
 */
@Controller
public class OrderWebController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData",confirmVo);
        return "confirm";
    }

    /**
     * 下单
     * @param orderSubmitVo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo orderSubmitVo, Model model, RedirectAttributes redirectAttributes) {
        try {
            OrderSubmitRespVo respVo = orderService.submitOrder(orderSubmitVo);
            if (respVo.getCode() == 0) {
                //下单成功到支付页
                model.addAttribute("submitOrderResp", respVo);
                return "pay";
            } else {
                String msg = "下单失败！";
                switch (respVo.getCode()) {
                    case 1:
                        msg += "订单信息过期，请刷新重试";
                        break;
                    case 2:
                        msg += "订单商品价格发生变化，请确认后再次提交";
                        break;
                }
                redirectAttributes.addFlashAttribute("msg", msg);
                return "redirect:http://order.gulimall.com/toTrade";
            }
        } catch (Exception e) {
            if (e instanceof NoStockException) {
                String msg = e.getMessage();
                redirectAttributes.addFlashAttribute("msg", msg);
            }
            return "redirect:http://order.gulimall.com/toTrade";
        }
    }
}
