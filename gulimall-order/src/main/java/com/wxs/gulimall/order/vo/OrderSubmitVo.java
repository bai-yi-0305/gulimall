package com.wxs.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author:Adolph
 * @create:2022/6/20
 */
@Data
public class OrderSubmitVo {
    /**
     * 收货地址id
     */
    private Long addrId;
    /**
     * 收获方式
     */
    private Integer payType;

    /**
     *  无需提交需要购买的商品，去购物车中获取就行
     */
    //优惠券，发票等。。。

    /**
     * 防重令牌
     */
    private String orderToken;
    /**
     * 应付价格
     */
    private BigDecimal payPrice;
    /**
     * 订单备注
     */
    private String note;
    /**
     * 用户相关信息直接去session中获取
     */
}
