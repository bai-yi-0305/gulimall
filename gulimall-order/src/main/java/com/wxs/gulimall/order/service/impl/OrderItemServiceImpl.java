package com.wxs.gulimall.order.service.impl;

import com.rabbitmq.client.Channel;
import com.wxs.gulimall.order.entity.OrderReturnApplyEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.order.dao.OrderItemDao;
import com.wxs.gulimall.order.entity.OrderItemEntity;
import com.wxs.gulimall.order.service.OrderItemService;


@RabbitListener(queues = {"hello-java-queue"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * queues：声明需要监听的队列
     * 接收参数类型
     * Message message：原生消息详细信息。头+体
     * T<发送消息时的类型>
     * Channel channel:传输数据的通道
     *
     * Queue：可以有很多人来监听。只要收到消息，队列删除消息，而且只能一个人收到消息
     * 场景：
     *      1、订单服务启动了多个：但是同一个消息只能被一个客户端收到
     *      2、只有一个消息完全处理完，方法运行结束，我们才可以接收下一个消息
     */
//    @RabbitListener(queues = {"hello-java-queue"})
    @RabbitHandler
    public void receive(Message message,
                        OrderReturnApplyEntity orderReturnApplyEntity,
                        Channel channel){
        System.out.println("obj = " + message);
        System.out.println("orderReturnApplyEntity = " + orderReturnApplyEntity);
        //通道内按顺序自增
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            //签收
            //channel.basicAck(deliveryTag,false);

            //拒签
            //long deliveryTag, boolean multiple, boolean requeue
            //requeue=true:返回服务器，重新入队
            //requeue=false:丢弃
            channel.basicNack(deliveryTag,false,true);

            //拒签，没有批量拒签
            //long deliveryTag, boolean requeue
            //channel.basicReject();

        } catch (IOException e) {
            //网络中断
            e.printStackTrace();
        }
    }

}