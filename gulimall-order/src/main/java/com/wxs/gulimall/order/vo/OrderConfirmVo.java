package com.wxs.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/6/19
 */
@Data
public class OrderConfirmVo {

    /**
     * 收货地址 ums_member_receive_address
     */
    private List<MemberAddressVo> address;

    /**
     * 所有选中的购物项信息
     */
    private List<OrderItemVo> items;

    /**
     * 发票记录
     */

    /**
     * 优惠卷
     */
    private Integer integration;

    /**
     * 订单方重令牌（防止页面重复提交数据）
     */
    private String orderToken;

    private Map<Long, Boolean> stocks;

    public Integer getCount(){
        if (items != null){
            return items.size();
        }
        return 0;
    }

    /**
     * 订单总额
     */
    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if (items != null){
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum =  sum.add(multiply);
            }
        }
        return sum;
    }

    /**
     * 应付总额
     */
    public BigDecimal getPayPrice() {
        return getTotal();
    }

    @Data
    public static class MemberAddressVo{

        private Long id;
        /**
         * member_id
         */
        private Long memberId;
        /**
         * 收货人姓名
         */
        private String name;
        /**
         * 电话
         */
        private String phone;
        /**
         * 邮政编码
         */
        private String postCode;
        /**
         * 省份/直辖市
         */
        private String province;
        /**
         * 城市
         */
        private String city;
        /**
         * 区
         */
        private String region;
        /**
         * 详细地址(街道)
         */
        private String detailAddress;
        /**
         * 省市区代码
         */
        private String areacode;
        /**
         * 是否默认
         */
        private Integer defaultStatus;
    }

    @Data
    public static class OrderItemVo{
        /**
         * 商品id
         */
        private Long skuId;
        /**
         * 商品标题
         */
        private String title;
        /**
         * 商品默认图片
         */
        private String defaultImage;
        /**
         * 商品sku信息(套餐信息)
         */
        private List<String> skuAttr;
        /**
         * 商品价格
         */
        private BigDecimal price;
        /**
         * 商品数量
         */
        private Integer count;
        /**
         * 商品小计价格
         */
        private BigDecimal totalPrice;

        /**
         * 商品重量
         */
        private BigDecimal weight;

    }
}
