package com.wxs.gulimall.order;


import com.wxs.gulimall.order.entity.OrderReturnApplyEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 1、如何创建Exchange、Queue、Binding
     *  （1）、使用AmqpAdmin进行创建
     * 2、如何首发消息
     */
    @Test
    public void createExchange() {
        //amqpAdmin
        amqpAdmin.declareExchange(new DirectExchange("hello-java-exchange",
                true,false));
        System.out.println("交换机创建成功");

    }

    @Test
    public void createQueue() {
        amqpAdmin.declareQueue(new Queue(
                "hello-java-queue",true,false,false));
        System.out.println("队列创建成功");
    }

    @Test
    public void createBinding() {
        /**
         * String destination:目的地
         * DestinationType destinationType：目的地类型
         * String exchange:交换机
         * String routingKey:路由键
         * Map<String, Object> arguments：参数
         */
        amqpAdmin.declareBinding(new Binding(
                "hello-java-queue", Binding.DestinationType.QUEUE,
                "hello-java-exchange","hello.java",null
        ));
        System.out.println("绑定成功");
    }

    @Test
    public void sendMessage() {
        OrderReturnApplyEntity orderReturnApplyEntity = new OrderReturnApplyEntity();
        orderReturnApplyEntity.setOrderId(1L);
        orderReturnApplyEntity.setCreateTime(new Date());
        orderReturnApplyEntity.setReturnName("ww");
        rabbitTemplate.convertAndSend("hello-java-exchange",
                "hello.java",orderReturnApplyEntity,new CorrelationData(UUID.randomUUID().toString()));
        System.out.println("消息发送完成");
    }
}
