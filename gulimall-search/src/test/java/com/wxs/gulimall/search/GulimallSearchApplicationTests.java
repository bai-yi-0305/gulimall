package com.wxs.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.wxs.gulimall.search.config.GulimallElasticsearchConfig;
import lombok.Data;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {

    @Resource
    private RestHighLevelClient client;


    @Test
    public void name() {
        String s = "500_";
        if (!s.startsWith("_") && !s.endsWith("_")){
            System.out.println("区间");
        }else if (s.startsWith("_")){
            System.out.println("_区间");
        }else {
            System.out.println("区间_");
        }
    }

    @Test
    public void testIndexData() throws IOException {
        IndexRequest request = new IndexRequest("users").id("1");
        //request.source("username","zs","age",20);
        User user = new User();
        user.setUserName("zs");
        user.setGender("男");
        user.setAge(20);
        request.source(JSON.toJSONString(user),XContentType.JSON);
        IndexResponse index = client.index(request, GulimallElasticsearchConfig.COMMON_OPTIONS);
        System.out.println("index = " + index);
    }

    @Test
    public void testSearch() throws IOException {
        SearchRequest request = new SearchRequest("users");
        request.source(new SearchSourceBuilder().query(QueryBuilders.matchAllQuery()));
        SearchResponse response = client.search(request, GulimallElasticsearchConfig.COMMON_OPTIONS);
        SearchHits hits = response.getHits();
        TotalHits totalHits = hits.getTotalHits();
        System.out.println("totalHits = " + totalHits);
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit searchHit : searchHits) {
            String source = searchHit.getSourceAsString();
            System.out.println("source = " + source);
        }
    }

    @Data
    class User{
        private String userName;
        private Integer age;
        private String gender;
    }

    @Test
    public void contextLoads() {
        System.out.println("client = " + client);
    }

}
