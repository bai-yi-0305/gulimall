package com.wxs.gulimall.search.controller;

import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.to.es.SkuEsModel;
import com.wxs.common.utils.R;
import com.wxs.gulimall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author:Adolph
 */
@Slf4j
@RequestMapping("search/save")
@RestController
public class ElasticSearchController {

    @Autowired
    private ProductSaveService productSaveService;

    @PostMapping("/product")
    private R productStatusUp(@RequestBody List<SkuEsModel> skuEsModel){
        boolean b = false;
        try {
            b = productSaveService.productStatusUp(skuEsModel);
        }catch (Exception e){
            log.error("ElasticSearchController商品上架错误:{}",e);
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
        if (!b){
            return R.ok();
        }else {
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
    }
}
