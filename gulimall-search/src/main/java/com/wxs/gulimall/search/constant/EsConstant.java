package com.wxs.gulimall.search.constant;

/**
 * @author:Adolph
 */
public class EsConstant {
    /**
     * sku数据在es中保存的索引名
     */
    public static final String PRODUCT_INDEX = "gulimall_product";

    public static final Integer PRODUCT_PAGE_SIZE = 8;
}
