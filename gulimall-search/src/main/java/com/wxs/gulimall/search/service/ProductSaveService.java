package com.wxs.gulimall.search.service;

import com.wxs.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author:Adolph
 */
public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModel> skuEsModel) throws IOException;
}
