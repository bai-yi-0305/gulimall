package com.wxs.gulimall.search.vo;

import com.wxs.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.Calendar;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/30
 * 查询返回结果
 */
@Data
public class SearchResult {
    /**
     * 查到的所有商品信息
     */
    private List<SkuEsModel> products;
    /**
     * 分页信息
     *  pageNum:当前第几页
     *  total:总记录数
     *  totalPages:总页码
     *  pageNaves:所有可遍历的导航页码
     *
     */
    private Integer pageNum;
    private Long total;
    private Integer totalPages;
    private List<Integer> pageNaves;
    /**
     * 当前查询到的结果，所有涉及到的所有品牌
     */
    private List<BrandVo> brands;
    /**
     * 当前查询到的结果，所有涉及到的所有属性
     */
    private List<AttrVo> attrs;
    /**
     * 当前查询到的结果，所有涉及到的所有分类
     */
    private List<CatelogVo> catelogs;

    //------------------------>以上都是返回给页面的信息<------------------------//

    /**
     * 面包屑导航数据
     */
//    private List<NavVo> navs;
//
//    @Data
//    public static class NavVo {
//        private String navName;
//        private String navValue;
//        private String link;
//    }


    @Data
    public static class CatelogVo{
        /**
         * 三级分类id
         */
        private Long catelogId;
        /**
         * 三级分类名
         */
        private String catelogName;
    }

    @Data
    public static class BrandVo{
        /**
         * 品牌id
         */
        private Long brandId;
        /**
         * 品牌名
         */
        private String brandName;
        /**
         * 品牌图片
         */
        private String brandImg;
    }

    @Data
    public static class AttrVo{
        /**
         * 属性id
         */
        private Long attrId;
        /**
         * 属性名
         */
        private String attrName;
        /**
         * 属性值，可能有多个
         */
        private List<String> attrValue;
    }
}
