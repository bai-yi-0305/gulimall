package com.wxs.gulimall.search.service;

import com.wxs.gulimall.search.vo.SearchParam;
import com.wxs.gulimall.search.vo.SearchResult;

/**
 * @author:Adolph
 * @create:2022/5/30
 */
public interface MallSearchService {

    SearchResult search(SearchParam searchParam);
}
