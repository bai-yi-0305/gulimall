package com.wxs.gulimall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.wxs.common.to.es.SkuEsModel;
import com.wxs.gulimall.search.config.GulimallElasticsearchConfig;
import com.wxs.gulimall.search.service.MallSearchService;
import com.wxs.gulimall.search.vo.SearchParam;
import com.wxs.gulimall.search.vo.SearchResult;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.wxs.gulimall.search.constant.EsConstant.PRODUCT_INDEX;
import static com.wxs.gulimall.search.constant.EsConstant.PRODUCT_PAGE_SIZE;

/**
 * @author:Adolph
 * @create:2022/5/30
 */
@Service
public class MallSearchServiceImpl implements MallSearchService {

    @Autowired
    private RestHighLevelClient client;

    /**
     * es中检索
     *
     * @param searchParam 请求参数
     * @return SearchResult对象
     */
    @Override
    public SearchResult search(SearchParam searchParam) {
        SearchResult searchResult = null;
        //1.准备请求
        SearchRequest request = buildSearchRequest(searchParam);
        try {
            //2.发送请求
            SearchResponse response = client.search(request, GulimallElasticsearchConfig.COMMON_OPTIONS);
            //3.解析结果
            searchResult = buildSearchResult(response, searchParam);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searchResult;
    }

    /**
     * 构建请求
     *
     * @param searchParam
     * @return
     */
    private SearchRequest buildSearchRequest(SearchParam searchParam) {
        //构件DSL
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        /**
         * 模糊匹配、过滤（属性、分类、品牌、价格区间、库存）
         */
        //1.bool--query
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(searchParam.getKeyword())) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("skuTitle", searchParam.getKeyword()));
        }
        //2.bool--filter catelogId
        if (searchParam.getCatelog3Id() != null) {
            boolQueryBuilder.filter(QueryBuilders.termQuery("catelogId", searchParam.getCatelog3Id()));
        }
        //2.bool--filter brandId
        if (!CollectionUtils.isEmpty(searchParam.getBrandId())) {
            boolQueryBuilder.filter(QueryBuilders.termsQuery("brandId", searchParam.getBrandId()));
        }
        //2.bool--filter 属性
        if (!CollectionUtils.isEmpty(searchParam.getAttrs())) {
            for (String attr : searchParam.getAttrs()) {
                BoolQueryBuilder builder = QueryBuilders.boolQuery();
                String[] strings = attr.split("_");
                String attrId = strings[0];//检索属性Id
                String[] attrValues = strings[1].split(":");//检索属性值
                builder.must(QueryBuilders.termQuery("attrs.attrId", attrId));
                builder.must(QueryBuilders.termsQuery("attrs.attrValue", attrValues));
                //每一个都必须生成一个nested查询
                NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("attrs", builder, ScoreMode.None);
                boolQueryBuilder.filter(nestedQueryBuilder);
            }
        }
        //2.bool--filter 库存
        boolQueryBuilder.filter(QueryBuilders.termQuery("hasStock", searchParam.getHasStock() == 1));

        //2.bool--filter 价格区间
        if (!StringUtils.isEmpty(searchParam.getSkuPrice())) {
            RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("skuPrice");
            if (!searchParam.getSkuPrice().startsWith("_") && !searchParam.getSkuPrice().endsWith("_")) {
                String[] strings = searchParam.getSkuPrice().split("_");
                rangeQuery.gte(strings[0]).lte(strings[1]);
            } else if (searchParam.getSkuPrice().equals("_")){
                rangeQuery.gte(null).lte(null);
            }else if (searchParam.getSkuPrice().startsWith("_")) {
                rangeQuery.lte(searchParam.getSkuPrice().split("_")[1]);
            } else {
                rangeQuery.gte(searchParam.getSkuPrice().split("_")[0]);
            }
            boolQueryBuilder.filter(rangeQuery);
        }
        sourceBuilder.query(boolQueryBuilder);
        /**
         * 排序、分页、高亮
         */
        //排序
        if (!StringUtils.isEmpty(searchParam.getSort())) {
            //sort=saleCount_asc/desc
            String[] strings = searchParam.getSort().split("_");
            SortOrder order = strings[1].equalsIgnoreCase("asc") ? SortOrder.ASC : SortOrder.DESC;
            sourceBuilder.sort(strings[0], order);
        }
        //分页
        sourceBuilder.from((searchParam.getPageNum() - 1) * PRODUCT_PAGE_SIZE);
        sourceBuilder.size(PRODUCT_PAGE_SIZE);
        //高亮
        if (!StringUtils.isEmpty(searchParam.getKeyword())) {
            HighlightBuilder builder = new HighlightBuilder();
            builder.field("skuTitle");
            builder.preTags("<b style='color:red'>");
            builder.postTags("</b>");
            sourceBuilder.highlighter(builder);
        }
        /**
         * 聚合分析
         */
        //品牌聚合
        TermsAggregationBuilder brandAgg = AggregationBuilders.terms("brandAgg").field("brandId").size(50);
        //字聚合
        brandAgg.subAggregation(AggregationBuilders.terms("brandNameAgg").field("brandName").size(1));
        brandAgg.subAggregation(AggregationBuilders.terms("brandImgAgg").field("brandImg").size(1));
        sourceBuilder.aggregation(brandAgg);
        //分类聚合
        TermsAggregationBuilder catelogAgg = AggregationBuilders.terms("catelogAgg").field("catelogId").size(20);
        catelogAgg.subAggregation(AggregationBuilders.terms("catelogNameAgg").field("catelogName").size(1));
        sourceBuilder.aggregation(catelogAgg);
        //属性聚合
        NestedAggregationBuilder attrAgg = AggregationBuilders.nested("attrAgg", "attrs");
        TermsAggregationBuilder attrIdAgg = AggregationBuilders.terms("attrIdAgg").field("attrs.attrId");
        attrIdAgg.subAggregation(AggregationBuilders.terms("attrNameAgg").field("attrs.attrName").size(1));
        attrIdAgg.subAggregation(AggregationBuilders.terms("attrValueAgg").field("attrs.attrValue").size(50));
        attrAgg.subAggregation(attrIdAgg);
        sourceBuilder.aggregation(attrAgg);

        String string = sourceBuilder.toString();
        System.out.println("string = " + string);

        SearchRequest request = new SearchRequest(new String[]{PRODUCT_INDEX}, sourceBuilder);
        return request;
    }

    /**
     * 解析响应结果
     *
     * @param response    es查询结果
     * @param searchParam
     * @return
     */
    private SearchResult buildSearchResult(SearchResponse response, SearchParam searchParam) {
        SearchResult searchResult = new SearchResult();
        SearchHits hits = response.getHits();
        List<SearchHit> searchHits = Arrays.asList(hits.getHits());
        if (!CollectionUtils.isEmpty(searchHits)) {
            List<SkuEsModel> products = searchHits.stream().map(source -> {
                String sourceAsString = source.getSourceAsString();
                SkuEsModel skuEsModel = JSON.parseObject(sourceAsString, SkuEsModel.class);
                if (!StringUtils.isEmpty(searchParam.getKeyword())){
                    HighlightField skuTitle = source.getHighlightFields().get("skuTitle");
                    String highlightString = skuTitle.getFragments()[0].string();
                    skuEsModel.setSkuTitle(highlightString);
                }
                return skuEsModel;
            }).collect(Collectors.toList());
            //1.返回所有查询到的商品
            searchResult.setProducts(products);
        }
        //2.返回所有商品涉及到的所有属性
        ParsedNested attrAgg = response.getAggregations().get("attrAgg");
        ParsedLongTerms attrIdAgg = attrAgg.getAggregations().get("attrIdAgg");
        List<SearchResult.AttrVo> attrVos = attrIdAgg.getBuckets().stream().map(bucket -> {
            SearchResult.AttrVo attrVo = new SearchResult.AttrVo();
            String attrId = bucket.getKeyAsString();
            attrVo.setAttrId(Long.parseLong(attrId));
            ParsedStringTerms attrNameAgg = bucket.getAggregations().get("attrNameAgg");
            String attrName = attrNameAgg.getBuckets().get(0).getKeyAsString();
            attrVo.setAttrName(attrName);
            ParsedStringTerms attrValueAgg = bucket.getAggregations().get("attrValueAgg");
            List<String> attrValues = attrValueAgg.getBuckets().stream().map(attrValue -> {
                return attrValue.getKeyAsString();
            }).collect(Collectors.toList());
            attrVo.setAttrValue(attrValues);
            return attrVo;
        }).collect(Collectors.toList());
        searchResult.setAttrs(attrVos);
        //3.返回所有商品涉及到的所有品牌
        ParsedLongTerms brandAgg = response.getAggregations().get("brandAgg");
        List<SearchResult.BrandVo> brandVos = brandAgg.getBuckets().stream().map(bucket -> {
            SearchResult.BrandVo brandVo = new SearchResult.BrandVo();
            String brandId = bucket.getKeyAsString();
            brandVo.setBrandId(Long.parseLong(brandId));
            ParsedStringTerms brandNameAgg = bucket.getAggregations().get("brandNameAgg");
            String brandName = brandNameAgg.getBuckets().get(0).getKeyAsString();
            brandVo.setBrandName(brandName);
            ParsedStringTerms brandImgAgg = bucket.getAggregations().get("brandImgAgg");
            String brandImages = brandImgAgg.getBuckets().get(0).getKeyAsString();
            brandVo.setBrandImg(brandImages);
            return brandVo;
        }).collect(Collectors.toList());
        searchResult.setBrands(brandVos);
        //4.返回所有商品涉及到的所有分类
        ParsedLongTerms catelogAgg = response.getAggregations().get("catelogAgg");
        List<? extends Terms.Bucket> buckets = catelogAgg.getBuckets();
        List<SearchResult.CatelogVo> catelogVos = buckets.stream().map(bucket -> {
            SearchResult.CatelogVo catelogVo = new SearchResult.CatelogVo();
            String key = bucket.getKeyAsString();
            catelogVo.setCatelogId(Long.parseLong(key));
            ParsedStringTerms catelogNameAgg = bucket.getAggregations().get("catelogNameAgg");
            String catelogName = catelogNameAgg.getBuckets().get(0).getKeyAsString();
            catelogVo.setCatelogName(catelogName);
            return catelogVo;
        }).collect(Collectors.toList());
        searchResult.setCatelogs(catelogVos);
        //------------------------------>以上信息聚合信息中获取<-----------------------------------
        //5.页码信息--当前页码
        searchResult.setPageNum(searchParam.getPageNum());
        //6.页码信息--总记录数
        long total = hits.getTotalHits().value;
        searchResult.setTotal(total);
        //7.页码信息--总页数--计算
        int totalPages = (int) (total % PRODUCT_PAGE_SIZE == 0 ?
                total / PRODUCT_PAGE_SIZE : (total / PRODUCT_PAGE_SIZE) + 1);
        searchResult.setTotalPages(totalPages);
        List<Integer> pageNaves = new ArrayList<>();
        for (int i = 1; i < totalPages;i++){
            pageNaves.add(i);
        }
        searchResult.setPageNaves(pageNaves);

        //构件面包屑导航
//        List<SearchResult.NavVo> navVos = searchParam.getAttrs().stream().map(attr -> {
//            SearchResult.NavVo navVo = new SearchResult.NavVo();
//
//            return navVo;
//        }).collect(Collectors.toList());
//        searchResult.setNavs(navVos);

        return searchResult;
    }
}
