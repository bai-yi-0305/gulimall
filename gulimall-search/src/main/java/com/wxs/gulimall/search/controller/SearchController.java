package com.wxs.gulimall.search.controller;

import com.wxs.gulimall.search.service.MallSearchService;
import com.wxs.gulimall.search.vo.SearchParam;

import com.wxs.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author:Adolph
 * @create:2022/5/30
 */
@Controller
public class SearchController {

    @Autowired
    private MallSearchService mallSearchService;

    /**
     * 自动将页面传递过来的参数封装成指定的对象
     * @param searchParam 指定接收参数的对象
     * @return
     */
    @GetMapping("/list.html")
    public String indexPath(SearchParam searchParam, Model model){

        SearchResult result = mallSearchService.search(searchParam);
        model.addAttribute("result",result);
        return "list";
    }
}
