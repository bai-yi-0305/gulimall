package com.wxs.gulimall.search.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @author:Adolph
 * @create:2022/6/2
 */
@Slf4j
public class ThreadTest {
    public static ExecutorService executor = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //异步编排
        log.info("方法开始。。。start!!!!");
//        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
//            log.info("当前线程：{}", Thread.currentThread().getId());
//            int i = 10 / 2;
//            log.info("运行结果：{}", i);
//        }, executor);

        //方法完成后的感知
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            log.info("当前线程：{}", Thread.currentThread().getId());
//            int i = 10 / 0;
//            log.info("运行结果：{}", i);
//            return i;
//        }, executor).whenComplete((result,exception) -> {
//            //whenComplete虽然可以获取异常，但是无法修改返回数据
//            log.info("异步调用成功完成了。结果是：{}，异常是：{}",result,exception);
//        }).exceptionally((exception) -> {
//            //exceptionally可以感知异常，同时可以返回默认数据，自己确定
//            return 10;
//        });
//        log.info("方法结束。。。end!!!!，结果：{}",future.get());

        //方法完成之后的处理
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            log.info("当前线程：{}", Thread.currentThread().getId());
//            int i = 10 / 5;
//            log.info("运行结果：{}", i);
//            return i;
//        }, executor).handle((result,exception) -> {
//            if (result != null){
//                return result * 2;
//            }
//            if (exception != null){
//                return 0;
//            }
//            return 0;
//        });
//        log.info("方法结束。。。end!!!!，结果：{}",future.get());


        /**
         * 线程串行化：
         * 1、thenRunAsync:不能获取到上一步的执行结果，无返回值
         * .thenRunAsync(() -> {
         *             log.info("任务2启动了");
         *         }, executor);
         *
         * 2、thenAcceptAsync:可以接收上一步结果，无返回值
         * .thenAcceptAsync((result) -> {
         *             log.info("任务2启动了,上一步结果：{}",result);
         *         },executor);
         *
         * 3、thenApplyAsync:即能接收上一步的结果，又有返回值
         * .thenApplyAsync((result) -> {
         *             log.info("任务2启动了,上一步结果：{}", result);
         *             return result * 20;
         *         }, executor);
         */
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            log.info("当前线程：{}", Thread.currentThread().getId());
//            int i = 10 / 5;
//            log.info("运行结果：{}", i);
//            return i;
//        }, executor).thenApplyAsync((result) -> {
//            log.info("任务2启动了,上一步结果：{}", result);
//            return result * 20;
//        }, executor);
//        log.info("方法结束。。。end!!!!，结果：{}", future.get());


        /**
         * 合并异步任务
         * 1、两个都完成
         */
//        CompletableFuture<Object> future1 = CompletableFuture.supplyAsync(() -> {
//            log.info("任务一线程：{}", Thread.currentThread().getId());
//            int i = 10 / 5;
//            log.info("任务一线程结束，结果：{}", i);
//            return i;
//        }, executor);
//
//        CompletableFuture<Object> future2 = CompletableFuture.supplyAsync(() -> {
//            log.info("任务二线程：{}", Thread.currentThread().getId());
//            log.info("任务二线程结束");
//            return "hello";
//        }, executor);


//        future1.runAfterBothAsync(future2,() -> {
//            log.info("任务三开始。。。。");
//        },executor);
//        log.info("runAfterBothAsync方法结束。。。end!!!!");

//        future1.thenAcceptBothAsync(future2,(res1,res2) -> {
//            log.info("任务三开始。。。。,任务1结果：{}，任务2结果：{}",res1,res2);
//        },executor);
//        log.info("thenAcceptBothAsync方法结束。。。end!!!!");

//        CompletableFuture<String> future = future1.thenCombineAsync(future2, (res1, res2) -> {
//            log.info("任务三开始。。。。,任务1结果：{}，任务2结果：{}", res1, res2);
//            return res1 + "<---------------->" + res2;
//        }, executor);
//        log.info("thenCombineAsync方法结束。。。end!!!!,thenCombineAsync方法结果：{}",future.get());

        /**
         * 两个任务只要一个完成，就执行任务三
         * runAfterEitherAsync:不感知结果，不返回结果
         * acceptEitherAsync:感知结果，不返回结果
         * applyToEitherAsync:感知结果，有返回结果
         */
//        future1.runAfterEitherAsync(future2,() -> {
//            log.info("任务三开始。。。。");
//        },executor);
//        log.info("runAfterEitherAsync方法结束。。。end!!!!");

//        future1.acceptEitherAsync(future2,(res) -> {
//            log.info("任务三开始。。。。,方法结果：{}",res);
//        },executor);
//        log.info("acceptEitherAsync方法结束。。。end!!!!");

//        CompletableFuture<String> future = future1.applyToEitherAsync(future2, (res) -> {
//            log.info("任务三开始。。。。,方法结果：{}", res);
//            return res + "<<<<<<<<<<";
//        }, executor);
//        log.info("applyToEitherAsync方法结束。。。end!!!!,结果：{}",future.get());


        CompletableFuture<String> img = CompletableFuture.supplyAsync(() -> {
            log.info("查询商品图片信息");
            return "hello.jpg";
        }, executor);

        CompletableFuture<String> attr = CompletableFuture.supplyAsync(() -> {
            log.info("查询商品属性信息");
            return "黑色+256G";
        }, executor);

        CompletableFuture<String> desc = CompletableFuture.supplyAsync(() -> {
            log.info("查询商品介绍信息");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "华为";
        }, executor);

        //CompletableFuture<Void> future = CompletableFuture.allOf(img, attr, desc);
        CompletableFuture<Object> future = CompletableFuture.anyOf(img, attr, desc);
        future.get();//等待所有结果完成
        //log.info("allOf方法结束。。。end!!!!,img:{},attr:{},desc:{}",img.get(),attr.get(),desc.get());
        log.info("anyOf方法结束。。。end!!!!,{}",future.get());

    }
}


