package com.wxs.gulimall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/30
 * 接收页面所有可能传递的查询参数
 * catelogId=225&keyword=小米&sort=saleCount_asc
 *
 * 检索条件分析：
 *  1、全文检索：skuTitle --> keyword
 *  2、排序：saleCount(销量)、hotScore(热度评分)、skuPrice(价格)
 *  3、过滤：hasStock(是否有货)、skuPrice区间、brandId、catelogId、attrs
 *  4、聚合：attrs
 */
@Data
public class SearchParam {
    /**
     * 三级分类Id
     */
    private Long catelog3Id;
    /**
     * 页面搜索关键字
     */
    private String keyword;
    /**
     * 排序条件（销量saleCount、热度hotScore、价格skuPrice）
     * sort=saleCount_asc/desc
     * sort=skuPrice_asc/desc
     * sort=hotScore_asc/desc
     */
    private String sort;
    /**
     * 过滤条件
     * hasStock(是否有货)、skuPrice区间、brandId、catelogId、attrs
     *  hasStock=0/1(1有货、0无货)
     *  skuPrice=1_500(1到500)/_500(500以下)/500_(500以上)
     *  brandId=1&brandId=2...
     *  attrs=1_其它:安卓&attrs=2_5存:6存
     *
     */
    private Integer hasStock = 1;//是否有货,默认有货
    private String skuPrice; //价格区间
    /**
     * 过滤条件：品牌选择：brandId
     * 允许多选
     */
    private List<Long> brandId;
    /**
     * 属性条件attrs
     * 按照属性条件筛选
     */
    private List<String> attrs;
    /**
     * 页码,第几页,默认第一页
     */
    private Integer pageNum = 1;


}
