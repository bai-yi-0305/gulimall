package com.wxs.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.product.entity.SkuInfoEntity;
import com.wxs.gulimall.product.entity.SpuInfoEntity;
import com.wxs.gulimall.product.vo.Skus;
import com.wxs.gulimall.product.vo.thymeleaf.SkuItemVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author adolph
 * @email adolph@gmail.com
 * @date 2022-05-11 19:12:48
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkus(SpuInfoEntity id, List<Skus> skus);

    PageUtils queryPageByCondition(Map<String, Object> params);

    List<SkuInfoEntity> getSkuBySpuId(Long spuId);

    SkuItemVo getSkuItem(Long skuId) throws ExecutionException, InterruptedException;

    Map<Long, BigDecimal> getSkuPrice(String jsonId);
}

