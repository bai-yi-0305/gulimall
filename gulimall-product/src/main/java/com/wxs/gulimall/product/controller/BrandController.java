package com.wxs.gulimall.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.wxs.common.valid.AddGro;
import com.wxs.common.valid.UpdateGro;
import com.wxs.common.valid.UpdateStatusGro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wxs.gulimall.product.entity.BrandEntity;
import com.wxs.gulimall.product.service.BrandService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;

import javax.validation.Valid;


/**
 * 品牌
 *
 * @author adolph
 * @email sunlightcs@gmail.com
 * @date 2022-05-11 19:12:48
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:brand:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    //@RequiresPermissions("product:brand:info")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     * @Valid: 开启JSR303校验注解
     * BindingResult: 可以拿到出错字段的所有信息
     * @Validated: spring框架提供的注解，指定要校验那一组
     *      value:指定组
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:brand:save")
    public R save(@Validated(value = {AddGro.class}) @RequestBody BrandEntity brand /*, BindingResult bindingResult*/){
/*        if (bindingResult.hasErrors()){
            Map<String, String> map = new HashMap<>();
            //获取错误校验信息
            bindingResult.getFieldErrors().forEach((error)->{
                //拿到默认错误消息，配置了就返回自己的
                String message = error.getDefaultMessage();
                //获取错误的属性名字
                String field = error.getField();
                map.put(field,message);
            });
            return R.error(400,"提交数据不合法！").put("data",map);
        }else {
            brandService.save(brand);
        }*/
        brandService.save(brand);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:brand:update")
    public R update(@Validated(value = {UpdateGro.class})
                        @RequestBody BrandEntity brand){
		brandService.updateDetail(brand);

        return R.ok();
    }

    /**
     * 修改状态
     */
    @RequestMapping("/update/status")
    //@RequiresPermissions("product:brand:update")
    public R updateStatus(@Validated(value = {UpdateStatusGro.class})
                    @RequestBody BrandEntity brand){
        brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:brand:delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
