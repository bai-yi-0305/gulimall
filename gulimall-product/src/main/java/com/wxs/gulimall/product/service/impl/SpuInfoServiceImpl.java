package com.wxs.gulimall.product.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.wxs.common.constant.ProductConstant;
import com.wxs.common.to.SkuHasStockTo;
import com.wxs.common.to.SpuBoundsTo;
import com.wxs.common.to.es.SkuEsModel;
import com.wxs.common.utils.R;
import com.wxs.gulimall.product.entity.*;
import com.wxs.gulimall.product.feign.CouponFeignService;
import com.wxs.gulimall.product.feign.SearchFeignService;
import com.wxs.gulimall.product.feign.WareFeignService;
import com.wxs.gulimall.product.service.*;
import com.wxs.gulimall.product.vo.BaseAttrs;
import com.wxs.gulimall.product.vo.Bounds;
import com.wxs.gulimall.product.vo.Skus;
import com.wxs.gulimall.product.vo.SpuSaveVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SpuInfoDescService spuInfoDescService;

    @Autowired
    private SpuImagesService spuImagesService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    @Autowired
    private SkuInfoService skuInfoService;

    @Autowired
    private CouponFeignService couponFeignService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private WareFeignService wareFeignService;

    @Autowired
    private SearchFeignService searchFeignService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * TODO 高级部分继续完善
     *
     * @param spuSaveVo
     */
    @Transactional
    @Override
    public void saveSpuInfo(SpuSaveVo spuSaveVo) {
        //1、保存spu基本信息 pms_spu_info
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuSaveVo, spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        this.saveBaseSpuInfo(spuInfoEntity);
        //2、保存spu描述图片 pms_spu_info_desc
        SpuInfoDescEntity spuInfoDescEntity = new SpuInfoDescEntity();
        List<String> decript = spuSaveVo.getDecript();
        spuInfoDescEntity.setSpuId(spuInfoEntity.getId());
        spuInfoDescEntity.setDecript(String.join(",", decript));
        spuInfoDescService.saveSpuInfoDesc(spuInfoDescEntity);
        //3、保存spu图片集 pms_spu_images
        List<String> images = spuSaveVo.getImages();
        spuImagesService.saveImages(spuInfoEntity.getId(), images);
        //4、保存spu规格参数 pms_product_attr_value
        List<BaseAttrs> baseAttrs = spuSaveVo.getBaseAttrs();
        productAttrValueService.savaBaseAttrs(spuInfoEntity.getId(), baseAttrs);
        //5、远程调用 保存spu的积分信息 gulimall_sms -------> sms_spu_bounds
        Bounds bounds = spuSaveVo.getBounds();
        SpuBoundsTo spuBoundsTo = new SpuBoundsTo();
        BeanUtils.copyProperties(bounds, spuBoundsTo);
        spuBoundsTo.setSpuId(spuInfoEntity.getId());
        R r = couponFeignService.savaSpuBounds(spuBoundsTo);
        if (r.getCode() != 0) {
            log.error("远程保存spu积分信息失败");
        }
        //6、保存spu对应的sku信息
        //6.1、sku的基本信息  pms_sku_info
        //6.2、sku图片信息 pms_sku_images
        //6.3、sku的销售属性信息 pms_sku_sale_attr_value
        //6.4、sku的优惠、满减等信息 gulimall_sms ----> sms_sku_ladder/sms_sku_full_reduction/sms_member_price
        List<Skus> skus = spuSaveVo.getSkus();
        skuInfoService.saveSkus(spuInfoEntity, skus);


    }

    @Override
    public void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity) {
        this.baseMapper.insert(spuInfoEntity);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wrapper.and(condition -> {
                condition.eq("id", key).or().like("spu_name", key);
            });
        }
        String status = (String) params.get("status");
        if (!StringUtils.isEmpty(status)) {
            wrapper.eq("publish_status", status);
        }
        String brandId = (String) params.get("brandId");
        //前端brandId默认值为0，转为String后为"0",所以要判断"0".equals(brandId)
        if (!"0".equals(brandId) && !StringUtils.isEmpty(brandId)) {
            wrapper.eq("brand_id", brandId);
        }
        String catelogId = (String) params.get("catelogId");
        //前端catelogId默认值为0，转为String后为"0",所以要判断"0".equals(catelogId)
        if (!"0".equals(catelogId) && !StringUtils.isEmpty(catelogId)) {
            wrapper.eq("catelog_id", catelogId);
        }
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    @Override
    public void spuUp(Long spuId) {
        //查询当前sku的所有可以被用来检索的规格属性
        //1.查询spu下的所有规格属性
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueService.baseListforspu(spuId);
        //2.从这些规格属性中获取属性id
        List<Long> attrIds = productAttrValueEntities.stream().map(ProductAttrValueEntity::getAttrId).collect(Collectors.toList());
        //3.通过属性id和search_type=1查询可以被检索的属性
        List<AttrEntity> searchAttrs = attrService.list(new QueryWrapper<AttrEntity>()
                .in("attr_id", attrIds).eq("search_type", 1));
        //4.收集可以被检索的属性id
        List<Long> searchIds = searchAttrs.stream().map(AttrEntity::getAttrId).collect(Collectors.toList());
        //5.创建可以被检索的属性id集合，在所有规格属性中筛选出可以被检索的规格属性
        Set<Long> idS = new HashSet<>(searchIds);
        //6.拿到可以被检索的规格属性,并且对SkuEsModel.Attrs进行封装
        List<SkuEsModel.Attrs> skuEsModelAttrs = productAttrValueEntities.stream().filter(productAttrValueEntity -> {
            return idS.contains(productAttrValueEntity.getAttrId());
        }).map(productAttrValueEntity -> {
            SkuEsModel.Attrs attrs = new SkuEsModel.Attrs();
            BeanUtils.copyProperties(productAttrValueEntity, attrs);
            return attrs;
        }).collect(Collectors.toList());

        //查出spuId对应的所有sku信息，品牌名字等
        List<SkuInfoEntity> skuInfoEntities = skuInfoService.getSkuBySpuId(spuId);
        List<Long> skuIds = skuInfoEntities.stream().map(SkuInfoEntity::getSkuId).collect(Collectors.toList());
        // TODO 1.发送远程请求，库存系统查询是否有库存hasStock
        Map<Long, Boolean> stockMap = null;
        try {
            R r = wareFeignService.getSkuHasStock(skuIds);
            TypeReference<List<SkuHasStockTo>> skuStockVoTypeReference = new TypeReference<List<SkuHasStockTo>>() {};
            //将list装换成map
            stockMap = r.getData(skuStockVoTypeReference).stream().collect(
                    Collectors.toMap(SkuHasStockTo::getSkuId, SkuHasStockTo::getHasStock));
        } catch (Exception e) {
            log.error("库存远程查询失败，原因：{}", e);
        }

        Map<Long, Boolean> finalStockMap = stockMap;
        //封装每一个sku的信息
        List<SkuEsModel> skuEsModels = skuInfoEntities.stream().map(skuInfoEntity -> {
            //组装需要的数据
            SkuEsModel skuEsModel = new SkuEsModel();
            BeanUtils.copyProperties(skuInfoEntity, skuEsModel);
            /**
             * skuPrice、skuImg、hasStock、hotScore、brandName、brandImg、catelogName、
             *      Attrs{
             *          attrId、
             *          attrName、
             *          attrValue
             *      }
             */
            skuEsModel.setSkuPrice(skuInfoEntity.getPrice());
            skuEsModel.setSkuImg(skuInfoEntity.getSkuDefaultImg());
            //设置库存hasStock
            if (finalStockMap == null) {
                skuEsModel.setHasStock(true);
            } else {
                skuEsModel.setHasStock(finalStockMap.get(skuInfoEntity.getSkuId()));
            }
            // TO
            // TODO 热度评分 0，
            skuEsModel.setHotScore(0L);
            // 查询品牌和分类的名字信息
            BrandEntity brandEntity = brandService.getById(skuInfoEntity.getBrandId());
            skuEsModel.setBrandName(brandEntity.getName());
            skuEsModel.setBrandImg(brandEntity.getLogo());
            CategoryEntity categoryEntity = categoryService.getById(skuInfoEntity.getCatelogId());
            skuEsModel.setCatelogName(categoryEntity.getName());
            //设置检索属性
            skuEsModel.setAttrs(skuEsModelAttrs);
            return skuEsModel;
        }).collect(Collectors.toList());
        // 数据发给es进行保存 gulimall-search
        R r = searchFeignService.productStatusUp(skuEsModels);
        if (r.getCode()==0){
            //远程调用成功
            // 修改当前spu的状态
            this.baseMapper.updateSpuStatus(spuId, ProductConstant.StatusEnum.SPU_UP.getCode());
        }else {
            //远程调用失败
            //TODO 重复调用，接口幂等性问题 ，重试机制
        }
    }

    @Override
    public SpuInfoEntity getSpuInfoBySkuId(Long skuId) {
        SkuInfoEntity skuInfoEntity = skuInfoService.getOne(new QueryWrapper<SkuInfoEntity>().eq("sku_id", skuId));
        Long spuId = skuInfoEntity.getSpuId();
        return this.baseMapper.selectOne(new QueryWrapper<SpuInfoEntity>().eq("id", spuId));
    }

}