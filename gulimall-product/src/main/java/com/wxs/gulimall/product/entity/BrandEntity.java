package com.wxs.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.wxs.common.valid.AddGro;
import com.wxs.common.valid.ListValue;
import com.wxs.common.valid.UpdateGro;
import com.wxs.common.valid.UpdateStatusGro;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 * 
 * @author adolph
 * @email adolph@gmail.com
 * @date 2022-05-11 19:12:48
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@Null(message = "添加品牌时不用指定id",groups = {AddGro.class})
	@NotNull(message = "修改必须指定id",groups = {UpdateGro.class})
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 * @NotBlank： 带注释的元素不能为空，并且必须至少包含一个非空白字符。接受字符序列
	 */
	@NotBlank(message = "品牌名不能为空",groups = {AddGro.class,UpdateGro.class})
	private String name;
	/**
	 * 品牌logo地址
	 * @URL： 传的必须是一个url路径
	 *
	 */
	@NotEmpty(groups = {AddGro.class,UpdateGro.class})
	@URL(message = "logo必须是一个合法的地址",groups = {AddGro.class,UpdateGro.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(groups = {AddGro.class, UpdateStatusGro.class})
	@ListValue(value = {0,1},groups = {AddGro.class, UpdateStatusGro.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 * @Pattern： 自定义规则，可以是一个正则表达式
	 * @NotEmpty: 带注释的元素不能为 null 也不能为空。不能标注在integer类型上
	 * 				支持的类型有：
	 * 						CharSequence（评估字符序列的长度）
	 * 						Collection（评估集合大小）
	 * 						Map（评估映射大小）
	 * 						Array（评估数组长度）
	 */
	@NotEmpty(message = "检索首字母必须填写",groups = {AddGro.class,UpdateGro.class})
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索首字母必须是一个a-z或A-Z中的字母")
	private String firstLetter;
	/**
	 * 排序
	 * @Min: 表示最小值不能小于value的值
	 * @NotNull: 带注释的元素不能为空。接受任何类型。
	 */
	@NotNull(message = "排序字段必须填写",groups = {AddGro.class,UpdateGro.class})
	@Min(value = 0,message = "排序字段必须大于0")
	private Integer sort;

}
