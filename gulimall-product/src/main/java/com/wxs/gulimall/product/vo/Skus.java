/**
  * Copyright 2022 bejson.com 
  */
package com.wxs.gulimall.product.vo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2022-05-21 17:26:21
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Skus {

    /**
     * sku销售属性
     */
    private List<Attr> attr;
    /**
     * sku基本信息
     */
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;
    /**
     * sku图片信息
     */
    private List<Images> images;

    /**
     * "descar": ["星河银", "8GB+256GB"],
     */
    private List<String> descar;

    /**
     * 满几件 ：sms_sku_ladder
     */
    private int fullCount;

    /**
     * 打几折： sms_sku_ladder
     */
    private BigDecimal discount;

    /**
     * 打折状态是否参加其它优惠
     */
    private int countStatus;

    /**
     *  满多少 sms_sku_full_reduction
     */
    private BigDecimal fullPrice;

    /**
     * 减多少 sms_sku_full_reduction
     */
    private BigDecimal reducePrice;

    /**
     * 是否满减，参加其它优惠
     */
    private int priceStatus;
    /**
     *会员价格 sms_member_price
     */
    private List<MemberPrice> memberPrice;

}