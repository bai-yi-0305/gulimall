package com.wxs.gulimall.product.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/5/21
 */
@Data
public class CategoryBrandRelationVo {
    /**
     *获取分类关联的品牌 :/product/categorybrandrelation/brands/list
     */
    private Long brandId;
    private String brandName;
}
