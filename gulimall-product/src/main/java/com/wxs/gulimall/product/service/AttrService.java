package com.wxs.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.product.entity.AttrEntity;
import com.wxs.gulimall.product.vo.AttrInfoVo;
import com.wxs.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author adolph
 * @email adolph@gmail.com
 * @date 2022-05-11 19:12:48
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryAttr(Map<String, Object> params, Long catelogId, String attrType);

    AttrInfoVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attrVo);

    void removeAttrAndAttrRelation(List<Long> asList);
}

