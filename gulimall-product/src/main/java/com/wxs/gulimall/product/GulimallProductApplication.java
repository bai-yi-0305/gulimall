package com.wxs.gulimall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 1、整合MyBatis-Plus
 *      1）、导入依赖
 *      <dependency>
 *             <groupId>com.baomidou</groupId>
 *             <artifactId>mybatis-plus-boot-starter</artifactId>
 *             <version>3.2.0</version>
 *      </dependency>
 *      2）、配置
 *          1、配置数据源；
 *              1）、导入数据库的驱动。https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-versions.html
 *              2）、在application.yml配置数据源相关信息
 *          2、配置MyBatis-Plus；
 *              1）、使用@MapperScan
 *              2）、告诉MyBatis-Plus，sql映射文件位置
 *
 * 2、逻辑删除
 *  1）、配置全局的逻辑删除规则（省略）
 *  2）、配置逻辑删除的组件Bean（省略）
 *  3）、给Bean加上逻辑删除注解@TableLogic
 *
 * 3、JSR303
 *   1）、给Bean添加校验注解:javax.validation.constraints，并定义自己的message提示
 *   2)、开启校验功能@Valid
 *      效果：校验错误以后会有默认的响应；
 *   3）、给校验的bean后紧跟一个BindingResult，就可以获取到校验的结果
 *   4）、分组校验（多场景的复杂校验）
 *         1)、	@NotBlank(message = "品牌名必须提交",groups = {AddGroup.class,UpdateGroup.class})
 *          给校验注解标注什么情况需要进行校验
 *         2）、@Validated({AddGroup.class})
 *         3)、默认没有指定分组的校验注解@NotBlank，
 *          在分组校验情况@Validated({AddGroup.class})下不生效，只会在@Validated生效；
 *
 *   5）、自定义校验
 *      1）、编写一个自定义的校验注解
 *      2）、编写一个自定义的校验器 ConstraintValidator
 *      3）、关联自定义的校验器和自定义的校验注解
 *
 * @Documented
 * @Constraint(validatedBy = { ListValueConstraintValidator.class【可以指定多个不同的校验器，适配不同类型的校验】 })
 * @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
 * @Retention(RUNTIME)
 * public @interface ListValue {}
 *
 * 4、统一的异常处理
 * @ControllerAdvice
 *  1）、编写异常处理类，使用@ControllerAdvice。
 *  2）、使用@ExceptionHandler标注方法可以处理的异常。
 *
 *  5、整合springCache简化redis缓存开发
 *      1、引入缓存依赖
 *          spring-boot-starter-cache、spring-boot-starter-data-redis
 *      2、写配置
 *          (1)、自动配置了哪些？
 *              CacheAutoConfiguration会自动导入RedisCacheConfiguration
 *              RedisCacheConfiguration会自动配置好缓存管理器RedisManager
 *          (2)、配置文件中配置使用redis作为缓存
 *      3、测试使用缓存
 *          @Cacheable：触发缓存填充。 ：触发将数据保存到缓存的操作
 *          @CacheEvict：触发缓存逐出。 ：触发将数据从缓存中删除
 *          @CachePut：在不干扰方法执行的情况下更新缓存。 ：不影响方法执行的情况下更新缓存
 *          @Caching：重新组合要应用于方法的多个缓存操作。 ：组合以上多个缓存操作
 *          @CacheConfig：在类级别共享一些常见的缓存相关设置。 ：在类级别共享缓存的相同配置
 *          （1）、开启缓存功能：@EnableCaching
 *          （2）、只需要使用注解就能完成缓存操作
 *      4、原理：
 *          CacheAutoConfiguration --> RedisCacheConfiguration --> 自动配置了RedisCacheManager --> 初始化所有的缓存
 *          --> 决定每一个缓存使用什么配置 --> 如果RedisCacheConfiguration有，就是用已有的，没有就使用默认配置
 *          --> 想要改缓存配置，只需要给容器中放一个RedisCacheConfiguration即可
 *          --> 就会应用到当前RedisCacheManager管理的所有缓存分区中
 *      5、springCache的不足：
 *          （1）、读模式
 *              1、缓存穿透：查询一个null数据。解决：缓存空数据 cache-null-values: true
 *              2、缓存击穿：大量并发查询一个正好的过期的数据。解决：加锁 ？ 默认是没有加锁的,
 *                  设置@Cacheable(sync = true)，开启同步模式，加了本地锁，用来解决缓存击穿问题
 *              3、缓存雪崩：大量的key同时过期。解决：加随机时间。加过期时间 time-to-live: 3600000
 *          （2）、写模式（缓存与数据库数据的一致性问题）
 *              1、读写加锁：适用于读多写少的数据
 *              2、使用中间件canal，感知mysql的更新，从而更新缓存
 *              3、读多写多，直接去数据库查询就行
 *      总结：常规数据（读多写少、即时性、一致性要求不高的数据）：完全可以使用springCache
 *           特殊数据：特殊设计
 *
 */
@EnableRedisHttpSession
@EnableFeignClients(basePackages = "com.wxs.gulimall.product.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
