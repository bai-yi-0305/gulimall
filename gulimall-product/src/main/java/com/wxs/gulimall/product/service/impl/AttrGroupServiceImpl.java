package com.wxs.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.constant.ProductConstant;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;
import com.wxs.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.wxs.gulimall.product.dao.AttrDao;
import com.wxs.gulimall.product.dao.AttrGroupDao;
import com.wxs.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.wxs.gulimall.product.entity.AttrEntity;
import com.wxs.gulimall.product.entity.AttrGroupEntity;
import com.wxs.gulimall.product.service.AttrGroupService;
import com.wxs.gulimall.product.service.AttrService;
import com.wxs.gulimall.product.vo.CategoryAttrGroupAttrVo;
import com.wxs.gulimall.product.vo.thymeleaf.SkuItemVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private AttrService attrService;
    @Autowired
    private AttrGroupDao attrGroupDao;
    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils query(Map<String, Object> params, Long catelogId) {
        String key = (String) params.get("key");
        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>();
        if (!StringUtils.isEmpty(key)) {
            wrapper.and((obj) -> {
                obj.eq("attr_group_id", key).or().like("attr_group_name", key);
            });
        }
        if (catelogId == 0) {
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    wrapper
            );
            return new PageUtils(page);
        } else {
            wrapper.eq("catelog_id", catelogId);
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    wrapper
            );
            return new PageUtils(page);
        }
    }


    @Override
    public PageUtils getAttrGroupAttrNoRelation(Map<String, Object> params, Long attrgroupId) {
        //1.当前分组只能关联自己所属分类中的属性
        //1.1通过attrgroupId查询表pms_attr_group表中自己所属的分类
        AttrGroupEntity attrGroupEntity = this.getById(attrgroupId);

        //找到该分类下的所有分组
        List<AttrGroupEntity> attrGroupEntities = attrGroupDao.selectList(new QueryWrapper<AttrGroupEntity>()
                .eq("catelog_id", attrGroupEntity.getCatelogId()));
        //拿到这些分组的id
        List<Long> attrGroupIds = attrGroupEntities.stream().map(AttrGroupEntity::getAttrGroupId).collect(Collectors.toList());
        //找到这些分组下关联的所有属性
        List<AttrAttrgroupRelationEntity> entities = attrAttrgroupRelationDao.selectList(
                new QueryWrapper<AttrAttrgroupRelationEntity>().in("attr_group_id", attrGroupIds));

        //被其他分组所关联的所有属性
        List<Long> attrIds = entities.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
        //2.当前分组关联的属性只能是没有被其他分组所关联的
        //查询本分类下没有被关联的属性
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<AttrEntity>();
        if (CollectionUtils.isEmpty(attrIds)) {
            //attrIds为空：表示这些分组没有关联任何属性
            //此时查询条件为：属于本分类下，不为销售属性，没有被本分组关联的属性
            wrapper.eq("catelog_id", attrGroupEntity.getCatelogId())
                    .ne("attr_type", ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode());
        } else {
            //存在被其他分组关联的属性
            //此时查询条件为：本分类下、不为销售属性、没有被本分组关联、没有被其他而分组关联的属性
            wrapper = new QueryWrapper<AttrEntity>()
                    .eq("catelog_id", attrGroupEntity.getCatelogId())
                    .ne("attr_type", ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode())
                    .notIn("attr_id", attrIds);
        }
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wrapper.and(obj -> {
                obj.eq("attr_id", key).or().like("attr_name", key);
            });
        }
        IPage<AttrEntity> page = attrService.page(
                new Query<AttrEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryAttrGroupAttrVo> getCategoryWithAttrGroupAndAttr(Long catelogId) {
        //catelogId->attrGroupId   attrGroupId->attrId                    attrId->attrEntity
        //pms_attr_group --------> pms_attr_attrgroup_relation---------->  pms_attr
        List<AttrGroupEntity> attrGroupEntities = this.list(new QueryWrapper<AttrGroupEntity>()
                .eq("catelog_id", catelogId));
        return attrGroupEntities.stream().map(attrGroupEntity -> {
            CategoryAttrGroupAttrVo categoryAttrGroupAttrVo = new CategoryAttrGroupAttrVo();
            BeanUtils.copyProperties(attrGroupEntity, categoryAttrGroupAttrVo);
            List<AttrAttrgroupRelationEntity> attrAttrGroupRelationEntities =
                    attrAttrgroupRelationDao.selectList(
                            new QueryWrapper<AttrAttrgroupRelationEntity>()
                                    .eq("attr_group_id", attrGroupEntity.getAttrGroupId()));
            if (CollectionUtils.isEmpty(attrAttrGroupRelationEntities)){
                categoryAttrGroupAttrVo.setAttrs(Collections.emptyList());
            }else{
                List<Long> attrIds = attrAttrGroupRelationEntities.stream()
                        .map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
                List<AttrEntity> attrEntities = attrService.list(new QueryWrapper<AttrEntity>().in("attr_id", attrIds));
                categoryAttrGroupAttrVo.setAttrs(attrEntities);
            }
            return categoryAttrGroupAttrVo;
        }).collect(Collectors.toList());
    }

    @Override
    public List<SkuItemVo.SpuItemAttrGroupVo> getAttrGroupBySpuIdAndCatelogId(Long spuId, Long catelogId) {
        return attrGroupDao.getAttrGroupBySpuIdAndCatelogId(spuId,catelogId);
    }
}