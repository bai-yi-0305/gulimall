package com.wxs.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author:Adolph
 * @create:2022/5/28
 */
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对redisson的操作都使用redissonClient操作
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.158.184:6379")
                .setPassword("2417").setDatabase(2);
        return Redisson.create(config);
    }
}
