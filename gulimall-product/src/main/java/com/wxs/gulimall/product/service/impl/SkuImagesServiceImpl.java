package com.wxs.gulimall.product.service.impl;

import com.wxs.gulimall.product.vo.Images;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.product.dao.SkuImagesDao;
import com.wxs.gulimall.product.entity.SkuImagesEntity;
import com.wxs.gulimall.product.service.SkuImagesService;
import org.springframework.util.StringUtils;


@Service("skuImagesService")
public class SkuImagesServiceImpl extends ServiceImpl<SkuImagesDao, SkuImagesEntity> implements SkuImagesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuImagesEntity> page = this.page(
                new Query<SkuImagesEntity>().getPage(params),
                new QueryWrapper<SkuImagesEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuImages(Long skuId, List<Images> images) {
        List<SkuImagesEntity> skuImagesEntities = images.stream().map(img -> {
            SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
            skuImagesEntity.setSkuId(skuId);
            skuImagesEntity.setImgUrl(img.getImgUrl());
            skuImagesEntity.setDefaultImg(img.getDefaultImg());
            return skuImagesEntity;
        }).filter(img->{
            // 没有图片路径的无需保存
            //返回true需要，返回false剔除
            return !StringUtils.isEmpty(img.getImgUrl());
        }).collect(Collectors.toList());
        this.saveBatch(skuImagesEntities);
    }

}