/**
  * Copyright 2022 bejson.com 
  */
package com.wxs.gulimall.product.vo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2022-05-21 17:26:21
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class SpuSaveVo {

    private String spuName;
    private String spuDescription;
    private Long catelogId;
    private Long brandId;
    private BigDecimal weight;
    private int publishStatus;
    /**
     * 图片描述信息（保存url地址）
     */
    private List<String> decript;
    /**
     * 图片集
     */
    private List<String> images;
    /**
     * spu积分信息（sms数据库）
     */
    private Bounds bounds;
    /**
     *spu基本属性（规格参数）
     */
    private List<BaseAttrs> baseAttrs;

    /**
     * spu对应sku信息
     */
    private List<Skus> skus;

}