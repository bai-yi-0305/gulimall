package com.wxs.gulimall.product.config;

import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;

/**
 * @author:Adolph
 * @create:2022/5/29
 */
@EnableCaching
@Configuration
@EnableConfigurationProperties(value = CacheProperties.class)
public class MyCacheConfig {

    /**
     * 配置了RedisCacheConfiguration之后，
     *  配置文件中的配置就会失效，
     *  因为不配置会默认读取RedisCacheConfiguration.defaultCacheConfig()配置，但是现在配置了，就会使用我们自己的，
     *  所以需要在这里进行重新配置
     *
     *  1、原来和配置文件绑定的配置类是这样的：没有放到容器中
     *      @ConfigurationProperties(prefix = "spring.cache")
     *      public class CacheProperties {}
     *  2、要让它生效，需要加上注解 @EnableConfigurationProperties(value = CacheProperties.class),将其放到容器中
     *          (新版本在CacheAutoConfiguration中已经放入容器，无需自己开启)
     *
     * @return
     */
    @Bean
    public RedisCacheConfiguration redisCacheConfiguration(CacheProperties cacheProperties){
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
        config = config.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(
                new GenericJackson2JsonRedisSerializer()
        ));
        CacheProperties.Redis redisProperties = cacheProperties.getRedis();
        //将配置文件的中的配置配置到此处，才会生效
        if (redisProperties.getTimeToLive() != null) {
            config = config.entryTtl(redisProperties.getTimeToLive());
        }

        if (redisProperties.getKeyPrefix() != null) {
            config = config.prefixKeysWith(redisProperties.getKeyPrefix());
        }

        if (!redisProperties.isCacheNullValues()) {
            config = config.disableCachingNullValues();
        }

        if (!redisProperties.isUseKeyPrefix()) {
            config = config.disableKeyPrefix();
        }
        return config;
    }

}
