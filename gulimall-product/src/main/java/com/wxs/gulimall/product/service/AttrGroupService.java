package com.wxs.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.product.entity.AttrGroupEntity;
import com.wxs.gulimall.product.vo.CategoryAttrGroupAttrVo;
import com.wxs.gulimall.product.vo.thymeleaf.SkuItemVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author adolph
 * @email sunlightcs@gmail.com
 * @date 2022-05-11 19:12:48
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils query(Map<String, Object> params, Long catelogId);

    PageUtils getAttrGroupAttrNoRelation(Map<String, Object> params, Long attrgroupId);

    List<CategoryAttrGroupAttrVo> getCategoryWithAttrGroupAndAttr(Long catelogId);

    List<SkuItemVo.SpuItemAttrGroupVo> getAttrGroupBySpuIdAndCatelogId(Long spuId, Long catelogId);
}

