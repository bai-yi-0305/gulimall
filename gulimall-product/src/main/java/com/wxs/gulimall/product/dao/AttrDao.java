package com.wxs.gulimall.product.dao;

import com.wxs.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author adolph
 * @email sunlightcs@gmail.com
 * @date 2022-05-11 19:12:48
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
