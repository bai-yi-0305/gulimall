package com.wxs.gulimall.product.constant;

import java.util.UUID;

/**
 * @author:Adolph
 * @create:2022/5/27
 */
public class ProductRedisConstant {
    /**
     * 商品分类封装信息id
     */
    public static final String CATELOG_JSON_CACHE = "product:catelogJSON";
    /**
     * 解决本地锁在分布式情况下出现的问题，使用redis中的setnx命令
     * REDIS_LOCK为setnx命令的key
     */
    public static final String REDIS_LOCK_KEY = "lock";

    public static final String PRODUCT_REDISSON_LOCK = "product:redisson:catelogJson";
}
