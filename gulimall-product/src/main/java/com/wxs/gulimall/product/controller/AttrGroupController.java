package com.wxs.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.wxs.gulimall.product.entity.AttrEntity;
import com.wxs.gulimall.product.service.AttrAttrgroupRelationService;
import com.wxs.gulimall.product.service.CategoryService;
import com.wxs.gulimall.product.vo.AttrGroupRelationVo;
import com.wxs.gulimall.product.vo.CategoryAttrGroupAttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wxs.gulimall.product.entity.AttrGroupEntity;
import com.wxs.gulimall.product.service.AttrGroupService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;

/**
 * 属性分组
 *
 * @author adolph
 * @email adolph@gmail.com
 * @date 2022-05-11 19:12:48
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    ///product/attrgroup/{catelogId}/withattr
    @GetMapping("/{catelogId}/withattr")
    public R getCategoryWithAttrGroupAndAttr(@PathVariable("catelogId")Long catelogId){
        List<CategoryAttrGroupAttrVo> data = attrGroupService.getCategoryWithAttrGroupAndAttr(catelogId);
        return R.ok().put("data",data);
    }

    /**
     * 添加属性与分组关联关系
     * 路径：/product/attrgroup/attr/relation
     * @param attrGroupRelationVos 属性分组id和属性id
     * @return
     */
    @PostMapping("/attr/relation")
    public R saveAttRelation(@RequestBody AttrGroupRelationVo[] attrGroupRelationVos){
        attrAttrgroupRelationService.saveAttRelation(attrGroupRelationVos);
        return R.ok();
    }

    /**
     * 获取属性分组里面还没有关联的本分类里面的其他基本属性，方便添加新的关联
     * 路径：/product/attrgroup/{attrgroupId}/noattr/relation
     * @param params ：分页参数
     * @param attrgroupId ： 属性分组id
     * @return
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R getAttrGroupAttrNoRelation(@RequestParam Map<String, Object> params,
                                        @PathVariable("attrgroupId") Long attrgroupId){
        PageUtils page = attrGroupService.getAttrGroupAttrNoRelation(params,attrgroupId);
        return R.ok().put("page",page);
    }


    /**
     * 删除属性与分组的关联关系
     * 路径：/product/attrgroup/attr/relation/delete
     * @param attrGroupRelationVo： 接收参数实体类
     * @return
     */
    @PostMapping("/attr/relation/delete")
    public R deleteRelation(@RequestBody AttrGroupRelationVo[] attrGroupRelationVo) {
        attrAttrgroupRelationService.deleteRelation(attrGroupRelationVo);
        return R.ok();
    }

    /**
     * 获取属性分组的关联的所有属性
     * 路径：/product/attrgroup/{attrgroupId}/attr/relation
     * @param attrgroupId： 属性分组id
     * @return
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R getAttrGroupAttrRelation(@PathVariable("attrgroupId") Long attrgroupId) {
        List<AttrEntity> data = attrAttrgroupRelationService.getAttrGroupAttrRelation(attrgroupId);
        return R.ok().put("data", data);
    }


    /**
     * 获取分类属性分组
     * 路径：/product/attrgroup/list/{catelogId}
     */
    @RequestMapping("/list/{catelogId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId") Long catelogId) {
        PageUtils page = attrGroupService.query(params, catelogId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long[] path = categoryService.findCatelogPath(attrGroup.getCatelogId());
        attrGroup.setCatelogPath(path);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
