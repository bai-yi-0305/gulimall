package com.wxs.gulimall.product.exception;

import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/5/16
 * 集中处理异常
 */
@Slf4j
//@ResponseBody
//@ControllerAdvice(basePackages = "com.wxs.gulimall.product")
@RestControllerAdvice(basePackages = "com.wxs.gulimall.product.controller.BrandController")
public class GulimallProductException {

    /**
     * 精确匹配异常：MethodArgumentNotValidException，JSR303校验时可能会抛出
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handlerValidException(MethodArgumentNotValidException e){
        Map<String, String> map = new HashMap<>();
        log.error("数据校验出现问题:{},异常类型：{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        bindingResult.getFieldErrors().forEach((error)->{
            String message = error.getDefaultMessage();
            String field = error.getField();
            map.put(field,message);
        });
        return R.error(BizCodeEnum.VALID_EXCEPTION.getCode(),
                BizCodeEnum.VALID_EXCEPTION.getMsg()).put("data",map);
    }

    /**
     * 可以处理任何异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Throwable.class)
    public R handlerException(Throwable e){
        return R.error(BizCodeEnum.UNKNOWN_EXCEPTION.getCode(),
                BizCodeEnum.UNKNOWN_EXCEPTION.getMsg());
    }
}
