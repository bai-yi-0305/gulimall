package com.wxs.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.wxs.gulimall.product.entity.ProductAttrValueEntity;
import com.wxs.gulimall.product.service.ProductAttrValueService;
import com.wxs.gulimall.product.vo.AttrInfoVo;
import com.wxs.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wxs.gulimall.product.entity.AttrEntity;
import com.wxs.gulimall.product.service.AttrService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;



/**
 * 商品属性
 *
 * @author adolph
 * @email sunlightcs@gmail.com
 * @date 2022-05-11 19:12:48
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;
    @Autowired
    private ProductAttrValueService productAttrValueService;


    ///product/attr/update/{spuId}
    //修改商品规格
    @PostMapping("/update/{spuId}")
    public R updateSpu(@RequestBody List<ProductAttrValueEntity> productAttrValueEntities,
                       @PathVariable("spuId") Long spuId){
        productAttrValueService.updateSpu(productAttrValueEntities,spuId);
        return R.ok();
    }

    /**
     * 获取spu规格
     * /product/attr/base/listforspu/{spuId}
     * @param spuId
     * @return
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseListforspu(@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> data = productAttrValueService.baseListforspu(spuId);
        return R.ok().put("data",data);
    }

    /**
     * 查询基本属性和销售属性
     * attrType：表示是销售属性还是基本属性
     */
        @RequestMapping("/{attrType}/list/{catelogId}")
    //@RequiresPermissions("product:attr:list")
    public R baseList(@RequestParam Map<String, Object> params,
                      @PathVariable("catelogId") Long catelogId,
                      @PathVariable("attrType") String attrType){
        PageUtils page = attrService.queryAttr(params,catelogId,attrType);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 查询属性详情
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId){
		AttrInfoVo attr = attrService.getAttrInfo(attrId);

        return R.ok().put("attr", attr);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrVo attr){
		attrService.saveAttr(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attr:update")
    public R update(@RequestBody AttrVo attrVo){
		attrService.updateAttr(attrVo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeAttrAndAttrRelation(Arrays.asList(attrIds));

        return R.ok();
    }

}
