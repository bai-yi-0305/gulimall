package com.wxs.gulimall.product.vo.thymeleaf;

import com.wxs.gulimall.product.entity.SkuImagesEntity;
import com.wxs.gulimall.product.entity.SkuInfoEntity;
import com.wxs.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/2
 */
@Data
public class SkuItemVo {

    //1、查询sku基本信息
    private SkuInfoEntity info;

    //2、sku的图片信息
    private List<SkuImagesEntity> images;

    //3、获取spu的销售属性组合
    private List<SkuItemSaleAttrVo> saleAttr;

    //4、获取spu的介绍
    private SpuInfoDescEntity desc;

    //5、获取spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttrs;

    private boolean hasStock = true;


    @Data
    public static class SkuItemSaleAttrVo{
        private Long attrId;
        private String attrName;
        private List<AttrValueWithSkuIdVo> attrValues;
    }

    @Data
    public static class SpuItemAttrGroupVo{
        private String groupName;
        private List<SpuBaseAttrVo> attrs;
    }

    @Data
    public static class SpuBaseAttrVo{
        private String attrName;
        private String attrValue;
    }

    @Data
    public static class AttrValueWithSkuIdVo{
        private String attrValue;
        private String skuIds;
    }
}
