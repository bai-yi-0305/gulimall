package com.wxs.gulimall.product.web;

import com.wxs.gulimall.product.entity.CategoryEntity;
import com.wxs.gulimall.product.service.CategoryService;
import com.wxs.gulimall.product.vo.thymeleaf.Catelog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/5/26
 */
@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping({"/","/index.html"})
    public String indexPage(Model model){
        //TODO 查出所有的一级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        model.addAttribute("categorys",categoryEntities);
        return "index";
    }

    @GetMapping("/index/catelog.json")
    @ResponseBody
    public Map<String, List<Catelog2Vo>> getCategoryJson(){
        return categoryService.getCategoryJson();
    }

}
