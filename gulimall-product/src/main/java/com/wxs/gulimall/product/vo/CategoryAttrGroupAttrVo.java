package com.wxs.gulimall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.wxs.gulimall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/21
 */
@Data
public class CategoryAttrGroupAttrVo {
    /**
     * 获取分类下所有分组&关联属性
     */

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    /**
     * 该分组下的所有关联的属性
     */
    private List<AttrEntity> attrs;
}
