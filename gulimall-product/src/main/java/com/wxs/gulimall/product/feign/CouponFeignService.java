package com.wxs.gulimall.product.feign;

import com.wxs.common.to.SkuReductionTo;
import com.wxs.common.to.SpuBoundsTo;
import com.wxs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author:Adolph
 * @create:2022/5/21
 * 优惠卷远程服务
 */
@Component
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    /**
     * 保存sku积分信息
     * @param spuBoundsTo
     * @return
     */
    @PostMapping("/coupon/spubounds/save")
    R savaSpuBounds(@RequestBody SpuBoundsTo spuBoundsTo);

    /**
     * 保存sku的优惠、满减等信息
     * @param skuReductionTo
     * @return
     */
    @PostMapping("/coupon/skufullreduction/saveInfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
