package com.wxs.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.wxs.gulimall.product.service.CategoryBrandRelationService;
import com.wxs.gulimall.product.vo.thymeleaf.Catelog2Vo;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.ThreadUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.product.dao.CategoryDao;
import com.wxs.gulimall.product.entity.CategoryEntity;
import com.wxs.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.cache.annotation.CachePut;

import static com.wxs.gulimall.product.constant.ProductRedisConstant.*;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    /**
     * redis脚本，解锁lua脚本
     */
    public static final DefaultRedisScript<Long> UNLOCK_SCRIPT;

    //类加载是进行初始化脚本位置和返回值类型
    static {
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        //设置lua脚本位置
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        //lua脚本返回值类型
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //查询所有菜单
        List<CategoryEntity> entities = this.list();
        //查询一级菜单和其子菜单并组装tree结构
        return entities.stream().filter(category -> category.getParentCid() == 0)
                .map((menu) -> {
                    menu.setChildren(getChildren(menu, entities));
                    return menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                })
                .collect(Collectors.toList());
    }

    @Override
    public void removeMenusByIds(List<Long> ids) {
        // TODO 检查删除节点是否被别的地方引用
        //逻辑删除，数据库show_status字段为1表示展示，0表示不展示
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        //递归。。。。
        List<Long> paths = new ArrayList<>();
        findAllPath(catelogId, paths);
        Collections.reverse(paths);
        return paths.toArray(new Long[0]);
    }

    /**
     * 更新分类的同时更新关联关系表
     * @CacheEvict： 失效模式，更新数据时，删除原来的缓存数据
     *  value:删除个缓存分区
     *  key：分区中的哪一个数据
     *  allEntries:默认为false，设为true，同时删除指定分区下的所有数据
     *  存储同一类数据，都可以指定成同一个分区。分区名默认都是缓存的前缀
     *
     * @Caching: 同时将读个缓存操作进行组合操作
     *
     * @param category
     */
    //@CacheEvict(value = "category",key = "'level1Categorys'")
//    @Caching(evict = {
//            @CacheEvict(value = "category",key = "'level1Categorys'"),
//            @CacheEvict(value = "category",key = "'getCategoryJson'")
//    })
    @CacheEvict(value = "category",allEntries = true) //清除模式使用
    //@CachePut //双写模式使用
    @Transactional
    @Override
    public void updateDetail(CategoryEntity category) {
        this.updateById(category);
        String name = category.getName();
        if (!StringUtils.isEmpty(name)) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        }
        //同时修改缓存数据
    }

    /**
     * 1、每一个需要缓存的数据我们都需要指定放到哪一个名字的缓存。【缓存的分区（按照业务类型分区）】
     * 2、@Cacheable({"category","product"})
     *          代表当前方法的结果是可以缓存的，如果缓存有，方法不用调用，缓存中没有，调用方法，最后结果放入缓存
     * 3、默认行为：
     *      （1）、如果缓存中有，方法不会调用
     *      （2）、key默认自动生成，包含:缓存的名字::SimpleKey [](自定指定)（category::SimpleKey []）
     *      （3）、缓存的value值：默认使用jdk序列化机制，将序列化后的数据存到redis
     *      （4）、默认过期时间ttl:-1，即永不过期
     *
     * 4、自定义：
     *      （1）、指定生成的缓存key：使用key属性指定，接收一个SpEL
     *          SpEL的使用规则：参考以下网站（Available Caching SpEL Evaluation Context）
     *              https://docs.spring.io/spring-framework/docs/current/reference/html/integration.html#cache-spel-context
     *      （2）、指定缓存数据存活时间 ：在配置文件中使用 spring.cache.redis.time-to-live(毫秒)指定
     *      （3）、将数据序列化为json格式
     */
    @Cacheable(value = {"category"},key = "'level1Categorys'"/*key = "#root.method.name"*/)
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        return this.baseMapper.selectList(
                new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
    }


    @Cacheable(value = "category",key = "#root.methodName",sync = true)
    @Override
    public Map<String, List<Catelog2Vo>> getCategoryJson() {
        //查询所有分类
        List<CategoryEntity> allCategoryList = this.baseMapper.selectList(null);
        //1.查询所有一级分类
        List<CategoryEntity> level1Categorys = getSonCategoryList(allCategoryList, 0L);
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //查询每一个一级分类的二级分类
            List<CategoryEntity> category2Entities = getSonCategoryList(allCategoryList, v.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (!CollectionUtils.isEmpty(category2Entities)) {
                //对二级分类封装成指定的Vo
                catelog2VoList = category2Entities.stream().map(categoryEntity2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo();
                    catelog2Vo.setCatelog1Id(v.getCatId().toString());
                    catelog2Vo.setId(categoryEntity2.getCatId().toString());
                    catelog2Vo.setName(categoryEntity2.getName());
                    //封装二级分类Vo中的三级分类Vo
                    //查询每一个二级分类对应的三级分类
                    List<CategoryEntity> category3Entities = getSonCategoryList(allCategoryList, categoryEntity2.getCatId());
                    //对查出的三级分类数据进行封装，称为Catelog2Vo.Catelog3Vo类型数据
                    List<Catelog2Vo.Catelog3Vo> catelog3VoList = category3Entities.stream().map(categoryEntity3 -> {
                        Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo();
                        catelog3Vo.setCatelog2Id(categoryEntity2.getCatId().toString());
                        catelog3Vo.setId(categoryEntity3.getCatId().toString());
                        catelog3Vo.setName(categoryEntity3.getName());
                        return catelog3Vo;
                    }).collect(Collectors.toList());
                    catelog2Vo.setCatelog3List(catelog3VoList);
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2VoList;
        }));
        return map;
    }

    /**
     * TODO OutOfDirectMemoryError:产生堆外内存溢出异常
     * 1）、springboot2.0以后默认使用lettuce操作redis客户端，它使用netty进行网络通信
     * 2）、lettuce的bug导致netty堆外内存溢出，-Xmx300m;netty如果没有指定堆外内存，默认使用-Xmx300m，自己指定的
     * 可以通过-Dio.netty.maxDirectMemory进行设置
     * 解决方案：不能使用-Dio.netty.maxDirectMemory只去调大堆外内存
     * 1）、升级lettuce客户端
     * 2）、切换使用jedis
     */
    //getCategoryJsonWithRedis（-> getCategoryJson）
    public Map<String, List<Catelog2Vo>> getCategoryJsonWithRedis() {
        /**
         * 1.缓存空结果：解决缓存穿透问题
         * 2.设置随机过期时间：解决缓存雪崩问题
         * 3.加锁：解决缓存击穿问题
         */
        //1.加入缓存逻辑
        String catelogJson = stringRedisTemplate.opsForValue().get(CATELOG_JSON_CACHE);
        if (StringUtils.isEmpty(catelogJson)) {
            //2.缓存中没有数据，查询数据库
            //返回
            return getCategoryJsonFromDBWithRedissonLock();
            //3.将查到的数据放入缓存,将对象转为JSON存到缓存，并设置过期时间（写到同步代码块中了！）
            //stringRedisTemplate.opsForValue().set(CATELOG_JSON_CACHE, JSON.toJSONString(categoryJsonFromDB), 1, TimeUnit.DAYS);
        }
        //将查出的结果转为指定的对象,复杂类型使用TypeReference指定类型
        return JSON.parseObject(catelogJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });
    }

    //从数据库中查询并封装分类数据,使用redisson加锁简化redis加锁开发
    public Map<String, List<Catelog2Vo>> getCategoryJsonFromDBWithRedissonLock() {
        //1.使用redisson获取锁
        RLock lock = redissonClient.getLock(PRODUCT_REDISSON_LOCK);
        lock.lock();
        Map<String, List<Catelog2Vo>> catelogJson;
        try {
            catelogJson = getCatelogDataAndSetRedis();
        } finally {
           lock.unlock();
        }
        //返回数据
        return catelogJson;
    }

    //从数据库中查询并封装分类数据,使用redis锁解决本地锁在分布式情况下出现的问题
    public Map<String, List<Catelog2Vo>> getCategoryJsonFromDBWithRedisLock() {
        /**
         * 解决分布式下本地锁出现的问题，使用redis中的setnx命令 + ex(过期时间)，两者一起设置，保证两个操作是原子的
         */
        //锁的value
        String redisLockValue = UUID.randomUUID().toString();
//        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent(REDIS_LOCK_KEY, redisLockValue);
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent(REDIS_LOCK_KEY, redisLockValue, 100, TimeUnit.SECONDS);
        if (BooleanUtils.isTrue(lock)) {
            System.out.println("--------------->加锁成功<-----------------");
            //加锁成功，设置过期时间,但是这里设置过期时间可能出现问题，加锁和设置过期时间不是一个原子操作，所以在加锁是同时设置过期时间
            //stringRedisTemplate.expire(REDIS_LOCK_KEY,30,TimeUnit.SECONDS);
            //Map<String, List<Catelog2Vo>> catelogJson = getCatelogDataAndSetRedis();
            /**
             * 问题一：直接解锁，解的锁可能不是自己所持有的锁：可能由于线程切换、阻塞导致还未释放锁，
             *                  但是锁的有效期到了，自动释放锁，然后别的线程拿到了锁之后，线程恢复了，
             *                  此时再去解锁，就出现了解的锁可能不是自己所持有的锁情况。
             *  解决技术：解锁之前先判断当前锁的value值是否和自己当时锁的value相等。
             *
             *   这个解决方案还会出现：问题二中的问题！！！！！
             *
             *   问题代码：
             *      //解锁 (不可以直接删除，可能不是自己的所持有的锁)
             *      //stringRedisTemplate.delete(REDIS_LOCK_KEY);
             */

            /**
             *问题二：
             *  在获取锁的value值时，虽然此时拿到的是属于自己的value，但是可能在请求和返回数据的过程中，有效期到了，
             *      此时自动释放了锁，别的线程拿到了锁，设置了自己的value值，这个时候才去进行判断
             *      if (redisLockValue.equals(lockValue)){}，结果任然回事true，还回去删除，这时候删除的确实别的线程的
             *      value值，所以还是问题。
             *      要解决这个问题就要保证获取锁的value值和删除操作是一个原子操作。
             *  使用技术：lua脚本
             *
             *  问题代码：
             *     //删除所之前先拿到锁的value，判断和自己的value是否一样，一样才删（这里也存在原子操作的问题，使用lua脚本解决）
             *     // lockValue = stringRedisTemplate.opsForValue().get(REDIS_LOCK_KEY);
             *     //if (redisLockValue.equals(lockValue)){
             *          //同一把锁，删除
             *     //    stringRedisTemplate.delete(REDIS_LOCK_KEY);
             *     //}
             */
            Map<String, List<Catelog2Vo>> catelogJson;
            try {
                catelogJson = getCatelogDataAndSetRedis();
            } finally {
                //解决问题一，问题二
                //使用 lua 脚本解决解锁是出现的原子问题
                stringRedisTemplate.execute(
                        UNLOCK_SCRIPT,
                        Collections.singletonList(REDIS_LOCK_KEY),
                        redisLockValue);
            }
            //返回数据
            return catelogJson;
        } else {
            System.out.println("--------------->加锁失败重试<-----------------");
            //加锁失败
            //休眠100ms重试
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCategoryJsonFromDBWithRedisLock();//自旋重试
        }
    }

    //从数据库中查询并封装分类数据,使用本地锁解决缓存击穿问题，但是在分布式情况下任有问题（redis：set nx ex 命令解决）
    public Map<String, List<Catelog2Vo>> getCategoryJsonFromDBWithLocalLock() {
        /**
         *加锁解决redis缓存击穿问题：
         * 1.synchronized (this)：springboot所有组件都是单实例的。
         * TODO 本地锁：synchronized、JUC（lock）,在分布式情况下，想要锁住所有，必须使用分布式锁
         */
        synchronized (this) {
            //得到锁以后，应该再次取redis中查询一次该数据，如果有直接返回，没有才需要再次查询
            return getCatelogDataAndSetRedis();
        }
    }

    private Map<String, List<Catelog2Vo>> getCatelogDataAndSetRedis() {
        //得到锁以后，应该再次取redis中查询一次该数据（可能不是第一个进来的线程），如果有直接返回，没有才需要再次查询
        String catelogJson = stringRedisTemplate.opsForValue().get(CATELOG_JSON_CACHE);
        if (!StringUtils.isEmpty(catelogJson)) {
            //缓存不为null,存在，直接返回
            return JSON.parseObject(catelogJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
        }
        //System.out.println("------------>查询了数据库<-----------");
        /**
         * 优化
         * 1、将数据库的多次查询变为一次
         */
        //查询所有分类
        List<CategoryEntity> allCategoryList = this.baseMapper.selectList(null);
        //1.查询所有一级分类
//        List<CategoryEntity> level1Categorys = this.getLevel1Categorys();
        List<CategoryEntity> level1Categorys = getSonCategoryList(allCategoryList, 0L);
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //查询每一个一级分类的二级分类
//            List<CategoryEntity> category2Entities = this.baseMapper.selectList(
//                    new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
            List<CategoryEntity> category2Entities = getSonCategoryList(allCategoryList, v.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (!CollectionUtils.isEmpty(category2Entities)) {
                //对二级分类封装成指定的Vo
                catelog2VoList = category2Entities.stream().map(categoryEntity2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo();
                    catelog2Vo.setCatelog1Id(v.getCatId().toString());
                    catelog2Vo.setId(categoryEntity2.getCatId().toString());
                    catelog2Vo.setName(categoryEntity2.getName());
                    //封装二级分类Vo中的三级分类Vo
                    //查询每一个二级分类对应的三级分类
//                    List<CategoryEntity> category3Entities = this.baseMapper.selectList(
//                            new QueryWrapper<CategoryEntity>().eq("parent_cid", categoryEntity2.getCatId()));
                    List<CategoryEntity> category3Entities = getSonCategoryList(allCategoryList, categoryEntity2.getCatId());
                    //对查出的三级分类数据进行封装，称为Catelog2Vo.Catelog3Vo类型数据
                    List<Catelog2Vo.Catelog3Vo> catelog3VoList = category3Entities.stream().map(categoryEntity3 -> {
                        Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo();
                        catelog3Vo.setCatelog2Id(categoryEntity2.getCatId().toString());
                        catelog3Vo.setId(categoryEntity3.getCatId().toString());
                        catelog3Vo.setName(categoryEntity3.getName());
                        return catelog3Vo;
                    }).collect(Collectors.toList());
                    catelog2Vo.setCatelog3List(catelog3VoList);
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2VoList;
        }));
        //3.将查到的数据放入缓存,将对象转为JSON存到缓存，并设置过期时间
        //在这里设置数据是为了防止释放锁之后，还没有将数据写入缓存，别的线程就已经进入同步代码块中，从而再次查询数据库
        stringRedisTemplate.opsForValue().set(CATELOG_JSON_CACHE, JSON.toJSONString(map), 1, TimeUnit.DAYS);
        return map;
    }

    /**
     * 找到其对应的子分类（一级分类对应二级分类、二级分类对应三级分类）
     *
     * @param allCategoryList 所有分类
     * @param parentCid       父分类id
     * @return 该父分类下的子分类集合
     */
    private List<CategoryEntity> getSonCategoryList(List<CategoryEntity> allCategoryList, Long parentCid) {
        return allCategoryList.stream().filter(
                categoryEntity -> categoryEntity.getParentCid().equals(parentCid)).collect(Collectors.toList());
    }

    //165 22 1
    private void findAllPath(Long catelogId, List<Long> paths) {
        paths.add(catelogId);
        CategoryEntity entity = this.getById(catelogId);
        if (entity.getParentCid() != 0) {
            findAllPath(entity.getParentCid(), paths);
        }
    }

    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        return all.stream().filter((category) -> category.getParentCid() == root.getCatId())
                .map((level2Menu) -> {
                    level2Menu.setChildren(getChildren(level2Menu, all));
                    return level2Menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                }).collect(Collectors.toList());
    }

}