package com.wxs.gulimall.product.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/5/19
 */
@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
