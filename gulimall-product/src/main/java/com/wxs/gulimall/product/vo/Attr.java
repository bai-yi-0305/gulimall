/**
  * Copyright 2022 bejson.com 
  */
package com.wxs.gulimall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2022-05-21 17:26:21
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;

}