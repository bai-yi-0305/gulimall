package com.wxs.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.wxs.common.to.SkuReductionTo;
import com.wxs.common.utils.R;
import com.wxs.gulimall.product.entity.SkuImagesEntity;
import com.wxs.gulimall.product.entity.SpuInfoDescEntity;
import com.wxs.gulimall.product.entity.SpuInfoEntity;
import com.wxs.gulimall.product.feign.CouponFeignService;
import com.wxs.gulimall.product.service.*;
import com.wxs.gulimall.product.vo.Images;
import com.wxs.gulimall.product.vo.Skus;
import com.wxs.gulimall.product.vo.thymeleaf.SkuItemVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.product.dao.SkuInfoDao;
import com.wxs.gulimall.product.entity.SkuInfoEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Autowired
    private SkuImagesService skuImagesService;
    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;
    @Autowired
    private CouponFeignService couponFeignService;
    @Autowired
    private SpuInfoDescService spuInfoDescService;
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private ThreadPoolExecutor executor;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkus(SpuInfoEntity entity, List<Skus> skus) {
        if (!CollectionUtils.isEmpty(skus)) {
            skus.forEach(sku -> {
                String defaultImage = "";
                for (Images image : sku.getImages()) {
                    //找到默认图片
                    if (image.getDefaultImg() == 1) {
                        defaultImage = image.getImgUrl();
                    }
                }
                //skuName price skuTitle skuSubtitle
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(sku, skuInfoEntity);
                skuInfoEntity.setBrandId(entity.getBrandId());
                skuInfoEntity.setCatelogId(entity.getCatelogId());
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setSpuId(entity.getId());
                skuInfoEntity.setSkuDefaultImg(defaultImage);
                //保存sku信息
                this.save(skuInfoEntity);
                //保存之后拿到skuId
                Long skuId = skuInfoEntity.getSkuId();
                //保存sku图片（保存之前必须先保存sku基本信息，因为保存图片的时候要使用skuId）
                //6.2、sku图片信息 pms_sku_images
                skuImagesService.saveSkuImages(skuId, sku.getImages());
                //6.3、sku的销售属性信息 pms_sku_sale_attr_value
                skuSaleAttrValueService.saveSkuSaleAttrValue(skuId, sku.getAttr());
                //6.4、sku的优惠、满减等信息 gulimall_sms ----> sms_sku_ladder/sms_sku_full_reduction/sms_member_price
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtils.copyProperties(sku, skuReductionTo);
                skuReductionTo.setSkuId(skuId);
                if (skuReductionTo.getFullCount() > 0 || skuReductionTo.getFullPrice().compareTo(new BigDecimal("0")) > 0) {
                    R r = couponFeignService.saveSkuReduction(skuReductionTo);
                    if (r.getCode() != 0) {
                        log.error("远程保存sku的优惠、满减等信息失败");
                    }
                }
            });
        }
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SkuInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wrapper.and(condition -> {
                condition.eq("sku_id", key).or().like("sku_name", key);
            });
        }
        String brandId = (String) params.get("brandId");
        //前端brandId默认值为0，转为String后为"0",所以要判断"0".equals(brandId)
        if (!"0".equals(brandId) && !StringUtils.isEmpty(brandId)) {
            wrapper.eq("brand_id", brandId);
        }
        String catelogId = (String) params.get("catelogId");
        //前端catelogId默认值为0，转为String后为"0",所以要判断"0".equals(catelogId)
        if (!"0".equals(catelogId) && !StringUtils.isEmpty(catelogId)) {
            wrapper.eq("catelog_id", catelogId);
        }
        String min = (String) params.get("min");
        if (!StringUtils.isEmpty(catelogId)) {
            wrapper.ge("price", min);
        }
        String max = (String) params.get("max");
        if (!StringUtils.isEmpty(catelogId)) {
            BigDecimal bigDecimal = new BigDecimal(max);
            if (bigDecimal.compareTo(new BigDecimal("0")) > 0) {
                wrapper.le("price", max);
            }
        }
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    @Override
    public List<SkuInfoEntity> getSkuBySpuId(Long spuId) {
        return list(new QueryWrapper<SkuInfoEntity>().eq("spu_id", spuId));
    }

    @Override
    public SkuItemVo getSkuItem(Long skuId) throws ExecutionException, InterruptedException {
        SkuItemVo skuItemVo = new SkuItemVo();
        //使用异步编排处理
        CompletableFuture<SkuInfoEntity> infoFuture = CompletableFuture.supplyAsync(() -> {
            //1、查询sku基本信息
            SkuInfoEntity skuInfoEntity = this.getById(skuId);
            skuItemVo.setInfo(skuInfoEntity);
            return skuInfoEntity;
        }, executor);

        CompletableFuture<Void> skuItemFuture = infoFuture.thenAcceptAsync((res) -> {
            //3、获取spu的规格参数信息
            List<SkuItemVo.SpuItemAttrGroupVo> attrGroupVos = attrGroupService.getAttrGroupBySpuIdAndCatelogId(res.getSpuId(), res.getCatelogId());
            skuItemVo.setGroupAttrs(attrGroupVos);
        }, executor);

        CompletableFuture<Void> spuDescFuture = infoFuture.thenAcceptAsync((res) -> {
            //4、获取spu的介绍
            SpuInfoDescEntity descEntity = spuInfoDescService.getById(res.getSpuId());
            skuItemVo.setDesc(descEntity);
        }, executor);

        CompletableFuture<Void> saleAttrFuture = infoFuture.thenAcceptAsync((res) -> {
            //5、获取spu的销售属性组合
            List<SkuItemVo.SkuItemSaleAttrVo> skuItemSaleAttrVos = skuSaleAttrValueService.getSkuSaleAttrBySpuId(res.getSpuId());
            skuItemVo.setSaleAttr(skuItemSaleAttrVos);
        }, executor);

        CompletableFuture<Void> imgFuture = CompletableFuture.runAsync(() -> {
            //2、sku的图片信息
            List<SkuImagesEntity> skuImages = skuImagesService.getBaseMapper().selectList(new QueryWrapper<SkuImagesEntity>().eq("sku_id", skuId));
            skuItemVo.setImages(skuImages);
        }, executor);

        //等待所有任务都完成
        CompletableFuture.allOf(skuItemFuture,spuDescFuture,saleAttrFuture,imgFuture).get();
        return skuItemVo;
    }

    @Override
    public Map<Long, BigDecimal> getSkuPrice(String jsonId) {
        List<Long> skuIds = JSON.parseObject(jsonId,new TypeReference<List<Long>>(){});
        List<SkuInfoEntity> skuInfos = this.baseMapper.selectList(
                new QueryWrapper<SkuInfoEntity>().in("sku_id", skuIds));
        if (skuInfos != null){
            return skuInfos.stream().collect(Collectors.toMap(SkuInfoEntity::getSkuId, SkuInfoEntity::getPrice));
        }
        return null;
    }
}