package com.wxs.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wxs.gulimall.product.entity.BrandEntity;
import com.wxs.gulimall.product.entity.CategoryEntity;
import com.wxs.gulimall.product.service.BrandService;
import com.wxs.gulimall.product.service.CategoryService;
import com.wxs.gulimall.product.vo.CategoryBrandRelationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.product.dao.CategoryBrandRelationDao;
import com.wxs.gulimall.product.entity.CategoryBrandRelationEntity;
import com.wxs.gulimall.product.service.CategoryBrandRelationService;
import org.springframework.util.CollectionUtils;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {


    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryBrandRelationEntity> getCatelogList(Long brandId) {
        return list(new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id", brandId));
    }

    @Override
    public void querySave(CategoryBrandRelationEntity categoryBrandRelation) {
        BrandEntity brandEntity = brandService.getOne(
                new QueryWrapper<BrandEntity>().eq("brand_id", categoryBrandRelation.getBrandId()));
        CategoryEntity categoryEntity = categoryService.getOne(
                new QueryWrapper<CategoryEntity>().eq("cat_id", categoryBrandRelation.getCatelogId()));
        categoryBrandRelation.setBrandName(brandEntity.getName());
        categoryBrandRelation.setCatelogName(categoryEntity.getName());
        save(categoryBrandRelation);
    }

    @Override
    public void updateBrand(Long brandId, String name) {
        CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
        categoryBrandRelationEntity.setBrandId(brandId);
        categoryBrandRelationEntity.setBrandName(name);
        this.update(categoryBrandRelationEntity,
                new UpdateWrapper<CategoryBrandRelationEntity>()
                        .eq("brand_id", brandId));
    }

    @Override
    public void updateCategory(Long catId, String name) {
        CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
        categoryBrandRelationEntity.setCatelogId(catId);
        categoryBrandRelationEntity.setCatelogName(name);
        this.update(categoryBrandRelationEntity,
                new UpdateWrapper<CategoryBrandRelationEntity>()
                        .eq("catelog_id", catId));
    }

    @Override
    public List<CategoryBrandRelationVo> getCategoryBrandRelation(Long catId) {
        //通过分类id查询其下的品牌id
        List<CategoryBrandRelationEntity> categoryBrandRelationEntityList =
                this.list(new QueryWrapper<CategoryBrandRelationEntity>().eq("catelog_id", catId));
        if (CollectionUtils.isEmpty(categoryBrandRelationEntityList)) {
            return null;
        } else {
            List<Long> brandIds = categoryBrandRelationEntityList.stream().map(CategoryBrandRelationEntity::getBrandId).collect(Collectors.toList());
            List<BrandEntity> brandEntityList = brandService.list(new QueryWrapper<BrandEntity>().in("brand_id", brandIds));
            List<CategoryBrandRelationVo> list = new ArrayList<>();
            brandEntityList.forEach(brandEntity -> {
                CategoryBrandRelationVo categoryBrandRelationVo = new CategoryBrandRelationVo();
                categoryBrandRelationVo.setBrandId(brandEntity.getBrandId());
                categoryBrandRelationVo.setBrandName(brandEntity.getName());
                list.add(categoryBrandRelationVo);
            });
            return list;
        }
    }
}