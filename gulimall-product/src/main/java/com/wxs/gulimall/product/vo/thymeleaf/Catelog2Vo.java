package com.wxs.gulimall.product.vo.thymeleaf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/26
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Catelog2Vo {

    /**
     * 一级分类id
     */
    private String catelog1Id;

    /**
     * 三级分类集合
     */
    private List<Catelog3Vo> catelog3List;

    /**
     * 当前分类id（二级分类）
     */
    private String id;

    /**
     * 当前分类name
     */
    private String name;

    /**
     * 三级分类
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
        public static class Catelog3Vo {
        /**
         * 二级分类id
         */
        private String catelog2Id;
        /**
         * 当前分类id(三级分类)
         */
        private String id;
        /**
         * 当前分类name
         */
        private String name;
    }

}
