package com.wxs.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author adolph
 * @email adolph@gmail.com
 * @date 2022-05-11 19:12:48
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfoDesc(SpuInfoDescEntity spuInfoDescEntity);
}

