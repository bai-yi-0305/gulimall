package com.wxs.gulimall.product;

import com.wxs.gulimall.product.dao.AttrGroupDao;
import com.wxs.gulimall.product.dao.SkuSaleAttrValueDao;
import com.wxs.gulimall.product.entity.BrandEntity;
import com.wxs.gulimall.product.service.BrandService;
import com.wxs.gulimall.product.vo.thymeleaf.SkuItemVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void test1() {
        List<SkuItemVo.SkuItemSaleAttrVo> skuSaleAttrBySpuId = skuSaleAttrValueDao.getSkuSaleAttrBySpuId(9L);
        System.out.println("skuSaleAttrBySpuId = " + skuSaleAttrBySpuId);
    }

    @Test
    public void test() {
        List<SkuItemVo.SpuItemAttrGroupVo> group =
                attrGroupDao.getAttrGroupBySpuIdAndCatelogId(8L, 225L);
        System.out.println("group = " + group);
    }

    @Test
    public void redisson() {
        System.out.println(redissonClient);
    }

    @Test
    public void testRedisTemplate() {
        stringRedisTemplate.opsForValue().set("name","zs");
    }

    @Test
    public void contextLoads() {
        List<BrandEntity> list = brandService.list();
        list.forEach(brand -> {
            System.out.println(brand);
        });
    }


    /**
     * 1.引入oss-starter
     * 2.配置key，endpoint相关信息即可
     * 3.使用ossclient进行相关操作
     * @throws FileNotFoundException
     */
//    文件上传测试
//    @Test
//     public void testUpload() throws FileNotFoundException {
//        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
//        String endpoint = "oss-cn-beijing.aliyuncs.com";
//        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
//        String accessKeyId = "LTAI5t7adYG7zAX99FGpiPb8";
//        String accessKeySecret = "tznuIAz0d6WjuabpxIObNk2fROkaor";
//
//        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//
//        // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
//        InputStream inputStream = new FileInputStream("E:\\VSCode\\iu\\iu2.jpg");
//        // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
//        ossClient.putObject("wxs-gulimall", "iu2.jpg", inputStream);
//
//        // 关闭OSSClient。
//        ossClient.shutdown();
//
//        System.out.println("上传完成");
//    }

}
