package com.wxs.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:21:15
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

