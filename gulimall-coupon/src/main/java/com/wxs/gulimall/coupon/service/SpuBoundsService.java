package com.wxs.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:21:15
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

