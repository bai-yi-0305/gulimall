package com.wxs.gulimall.coupon.dao;

import com.wxs.gulimall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:21:15
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
