package com.wxs.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.coupon.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:21:15
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

