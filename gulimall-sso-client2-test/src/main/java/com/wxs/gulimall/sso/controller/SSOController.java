package com.wxs.gulimall.sso.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/16
 */
@Controller
public class SSOController {

    @Value("${ssoserver.redirect.url}")
    String url;

    @GetMapping("/boss")
    public String employees(Model model,
                            HttpSession session,
                            @RequestParam(value = "token",required = false) String token) {
        if (!StringUtils.isEmpty(token)){
            //TODO 去ssoserver.com获取登录用户信息
            session.setAttribute("loginUser","zs");
        }
        Object loginUser = session.getAttribute("loginUser");
        if (loginUser == null) {
            return "redirect:" + url + "?url=http://client1.com:8082/boss";
        } else {
            List<String> boss = new ArrayList<>();
            boss.add("ww");
            boss.add("ll");
            model.addAttribute("boss", boss);
            return "list";
        }
    }
}
