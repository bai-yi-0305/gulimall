package com.wxs.gulimall.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimallSsoClient2TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallSsoClient2TestApplication.class, args);
    }

}
