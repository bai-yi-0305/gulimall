package com.wxs.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.member.entity.MemberReceiveAddressEntity;

import java.util.List;
import java.util.Map;

/**
 * 会员收货地址
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:25:44
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<MemberReceiveAddressEntity> getMemberReceiveAddress(Long memberId);
}

