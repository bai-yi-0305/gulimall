package com.wxs.gulimall.member.feign;

import com.wxs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/6/26
 */
@FeignClient("gulimall-order")
public interface OrderFeignService {

    @PostMapping("/order/order/getOrderAndOrderItem")
    R getOrderAndOrderItem(@RequestBody Map<String, Object> params);
}
