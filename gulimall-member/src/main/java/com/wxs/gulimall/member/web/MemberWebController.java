package com.wxs.gulimall.member.web;

import com.alibaba.fastjson.JSON;
import com.wxs.common.utils.R;
import com.wxs.gulimall.member.feign.OrderFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/6/25
 */
@Controller
public class MemberWebController {

    @Autowired
    private OrderFeignService orderFeignService;

    @GetMapping("/memberOrder.html")
    public String memberList(@RequestParam(value = "pageNum",defaultValue = "1") String pageNum, Model model){
        Map<String,Object> map = new HashMap<>();
        map.put("page",pageNum);
        R r = orderFeignService.getOrderAndOrderItem(map);
        System.out.println(JSON.toJSONString(r));
        model.addAttribute("orders",r);
        return "orderList";
    }
}
