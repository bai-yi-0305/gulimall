package com.wxs.gulimall.member.exception;

/**
 * @author:Adolph
 * @create:2022/6/6
 */
public class UserNameExistException extends RuntimeException{
    public UserNameExistException() {
        super("用户名已存在");
    }
}
