package com.wxs.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.member.entity.MemberEntity;
import com.wxs.gulimall.member.vo.MemberLoginVo;
import com.wxs.gulimall.member.vo.MemberRegisterVo;
import com.wxs.gulimall.member.vo.OauthGiteeLoginVo;

import java.util.Map;

/**
 * 会员
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:25:44
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void register(MemberRegisterVo memberRegisterVo);

    MemberEntity login(MemberLoginVo memberLoginVo);

    MemberEntity oauth2LoginAndRegister(OauthGiteeLoginVo vo);
}

