package com.wxs.gulimall.member.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author:Adolph
 * @create:2022/6/19
 */
@Configuration
public class GuliFeignConfig {

    @Bean
    public RequestInterceptor requestInterceptor(){
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                //1、RequestContextHolder，请求上下文保持器，通过这个RequestContextHolder拿到刚进来的请求，里面保存了请求时的cookie
                ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if (attributes != null){
                    //2、获取刚进来时的请求(老的请求)
                    HttpServletRequest request = attributes.getRequest();
                    //3、获取请求头数据
                    String cookie = request.getHeader("Cookie");
                    //4、给新请求同步老请求头中的cookie数据
                    requestTemplate.header("Cookie",cookie);
                }
            }
        };
    }
}
