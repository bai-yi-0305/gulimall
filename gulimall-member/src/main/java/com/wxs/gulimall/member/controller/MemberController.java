package com.wxs.gulimall.member.controller;

import java.util.Arrays;
import java.util.Map;

import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.vo.MemberEntityVo;
import com.wxs.gulimall.member.exception.PhoneNumberExistException;
import com.wxs.gulimall.member.exception.UserNameExistException;
import com.wxs.gulimall.member.feign.CouponFeignService;
import com.wxs.gulimall.member.vo.MemberLoginVo;
import com.wxs.gulimall.member.vo.MemberRegisterVo;
import com.wxs.gulimall.member.vo.OauthGiteeLoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wxs.gulimall.member.entity.MemberEntity;
import com.wxs.gulimall.member.service.MemberService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;



/**
 * 会员
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:25:44
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    private CouponFeignService couponFeignService;

    @PostMapping("/oauth2/login")
    public R oauth2LoginAndRegister(@RequestBody OauthGiteeLoginVo vo){
        MemberEntity memberEntity =  memberService.oauth2LoginAndRegister(vo);
        return memberEntity != null ? R.ok().setData(memberEntity) : R.error(BizCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getCode(),
                BizCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getMsg());

    }

    @PostMapping("/login")
    public R login(@RequestBody MemberLoginVo memberLoginVo){
        MemberEntity member= memberService.login(memberLoginVo);
        return member != null ? R.ok().setData(member) : R.error(BizCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getCode(),
                BizCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getMsg());
    }


    @PostMapping("/register")
    public R register(@RequestBody MemberRegisterVo memberRegisterVo){
        try {
            memberService.register(memberRegisterVo);
        } catch (PhoneNumberExistException e) {
           return R.error(BizCodeEnum.PHONE_EXIST_EXCEPTION.getCode(), BizCodeEnum.PHONE_EXIST_EXCEPTION.getMsg());
        }catch (UserNameExistException e) {
            return R.error(BizCodeEnum.USERNAME_EXIST_EXCEPTION.getCode(), BizCodeEnum.USERNAME_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
