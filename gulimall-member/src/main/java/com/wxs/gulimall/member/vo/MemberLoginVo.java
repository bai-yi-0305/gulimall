package com.wxs.gulimall.member.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/7
 */
@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
