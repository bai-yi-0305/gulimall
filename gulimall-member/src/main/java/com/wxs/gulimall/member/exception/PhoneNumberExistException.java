package com.wxs.gulimall.member.exception;

/**
 * @author:Adolph
 * @create:2022/6/6
 */
public class PhoneNumberExistException extends RuntimeException{
    public PhoneNumberExistException() {
        super("手机号已存在");
    }
}
