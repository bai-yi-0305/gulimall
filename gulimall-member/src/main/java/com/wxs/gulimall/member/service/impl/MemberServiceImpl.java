package com.wxs.gulimall.member.service.impl;

import com.wxs.gulimall.member.entity.MemberLevelEntity;
import com.wxs.gulimall.member.exception.PhoneNumberExistException;
import com.wxs.gulimall.member.exception.UserNameExistException;
import com.wxs.gulimall.member.service.MemberLevelService;
import com.wxs.gulimall.member.vo.MemberLoginVo;
import com.wxs.gulimall.member.vo.MemberRegisterVo;
import com.wxs.gulimall.member.vo.OauthGiteeLoginVo;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;

import com.wxs.gulimall.member.dao.MemberDao;
import com.wxs.gulimall.member.entity.MemberEntity;
import com.wxs.gulimall.member.service.MemberService;
import org.springframework.util.StringUtils;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    private MemberLevelService memberLevelService;
    @Autowired
    private MemberService memberService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void register(MemberRegisterVo memberRegisterVo) {
        MemberEntity memberEntity = new MemberEntity();
        //设置默认等级
        MemberLevelEntity memberLevelEntity = memberLevelService.getOne(
                new QueryWrapper<MemberLevelEntity>().eq("default_status", 1));
        memberEntity.setLevelId(memberLevelEntity.getId());
        //检查手机号、用户名是否存在，存在就会抛出异常
        checkUserNameUnique(memberRegisterVo.getUserName());
        checkPhoneUnique(memberRegisterVo.getPhone());
        //走到这里说明不存在
        memberEntity.setUsername(memberRegisterVo.getUserName());
        memberEntity.setMobile(memberRegisterVo.getPhone());
        memberEntity.setNickname(memberRegisterVo.getUserName());
        //密码加密存储
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encodePassword = bCryptPasswordEncoder.encode(memberRegisterVo.getPassword());
        memberEntity.setPassword(encodePassword);
        //其它默认信息。。。

        //保存信息
        memberService.save(memberEntity);
    }

    @Override
    public MemberEntity login(MemberLoginVo memberLoginVo) {
        String loginacct = memberLoginVo.getLoginacct();
        String password = memberLoginVo.getPassword();
        MemberEntity memberEntity = this.baseMapper.selectOne(
                new QueryWrapper<MemberEntity>().eq("username", loginacct).or().eq("mobile", loginacct));
        if (memberEntity == null){
            return null;
        }else {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            boolean b = bCryptPasswordEncoder.matches(password, memberEntity.getPassword());
            return b ? memberEntity : null;
        }
    }

    @Override
    public MemberEntity oauth2LoginAndRegister(OauthGiteeLoginVo vo) {
        //登录和注册合并逻辑
        String giteeId = vo.getGiteeId();
        MemberEntity memberEntity = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("gitee_id", giteeId));
        if (memberEntity != null){
            //这个用户已经注册
            memberEntity.setAccessToken(vo.getAccessToken());
            this.baseMapper.updateById(memberEntity);
            return memberEntity;
        }
        //未注册
        MemberEntity insert = new MemberEntity();
        MemberLevelEntity memberLevelEntity = memberLevelService.getOne(
                new QueryWrapper<MemberLevelEntity>().eq("default_status", 1));
        insert.setLevelId(memberLevelEntity.getId());
        insert.setCreateTime(new Date());
        insert.setNickname(vo.getName());
        insert.setGiteeId(giteeId);
        insert.setAccessToken(vo.getAccessToken());
        this.baseMapper.insert(insert);
        return insert;
    }

    private void checkPhoneUnique(String phone) throws UserNameExistException {
        int count = memberService.count(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (count > 0) {
            throw new UserNameExistException();
        }
    }

    private void checkUserNameUnique(String userName) throws PhoneNumberExistException {
        int count = memberService.count(new QueryWrapper<MemberEntity>().eq("username", userName));
        if (count > 0) {
            throw new PhoneNumberExistException();
        }
    }
}