package com.wxs.gulimall.member.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/6
 */
@Data
public class MemberRegisterVo {
    private String userName;
    private String password;
    private String phone;
}
