package com.wxs.gulimall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/23
 */
@Data
public class PurchaseDoneVo {
    /**
     * 采购单id
     */
    @NotNull
    private Long id;
    /**
     * 该采购单下采购项集合
     */
    private List<PurchaseDetailVo> items;

}
