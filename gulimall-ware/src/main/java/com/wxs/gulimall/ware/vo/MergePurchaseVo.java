package com.wxs.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/5/22
 */
@Data
public class MergePurchaseVo {
    /**
     * 整单id
     */
    private Long purchaseId;
    /**
     * 合并项集合
     */
    private List<Long> items;
}
