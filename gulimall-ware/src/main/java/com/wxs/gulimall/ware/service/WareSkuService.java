package com.wxs.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.to.SkuHasStockTo;
import com.wxs.common.to.mq.OrderTo;
import com.wxs.common.to.mq.StockLockedTo;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.ware.entity.WareSkuEntity;
import com.wxs.gulimall.ware.to.WareSkuLockTo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:28:47
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCondition(Map<String, Object> params);

    void updateWareSku(Long skuId, Integer skuNum, Long wareId);

    List<SkuHasStockTo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockTo to);

    void unLockStock(StockLockedTo to);

    void unLockStock(OrderTo orderTo);
}

