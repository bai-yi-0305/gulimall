package com.wxs.gulimall.ware.to;

import lombok.Data;

import java.util.List;

/**
 * @author:Adolph
 * @create:2022/6/21
 */
@Data
public class WareSkuLockTo {

    /**
     * 订单号
     */
    private String orderSn;
    /**
     * 需要锁定的库存
     */
    private List<OrderItemTo> locks;
}
