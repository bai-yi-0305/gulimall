package com.wxs.gulimall.ware.vo;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/5/23
 */
@Data
public class PurchaseDetailVo {
    /**
     * 采购项id
     */
    private Long itemId;
    /**
     * 采购项状态
     */
    private Integer status;
    /**
     * 采购失败原因，成功为空
     */
    private String reason;
}
