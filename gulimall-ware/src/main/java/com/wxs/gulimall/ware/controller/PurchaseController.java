package com.wxs.gulimall.ware.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.wxs.gulimall.ware.vo.MergePurchaseVo;
import com.wxs.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.wxs.gulimall.ware.entity.PurchaseEntity;
import com.wxs.gulimall.ware.service.PurchaseService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;



/**
 * 采购信息
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:28:47
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    ///ware/purchase/done
    //完成采购
    @PostMapping("/done")
    public R done(@Validated @RequestBody PurchaseDoneVo purchaseDoneVo){
        purchaseService.done(purchaseDoneVo);
        return R.ok();
    }

    //领取采购单
    ///ware/purchase/received
    @PostMapping("/received")
    public R received(@RequestBody Long[] purchaseIds){
        purchaseService.received(purchaseIds);
        return R.ok();
    }

    ///ware/purchase/merge
    //合并采购需求
    @PostMapping("/merge")
    public R mergePurchase(@RequestBody MergePurchaseVo mergePurchaseVo){
        purchaseService.mergePurchase(mergePurchaseVo);
        return R.ok();
    }

    ///ware/purchase/unreceive/list
    //查看未领取采购单
    @RequestMapping("/unreceive/list")
    public R unReceivePurchase(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPageUnReceivePurchase(params);

        return R.ok().put("page", page);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:purchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:purchase:info")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:purchase:save")
    public R save(@RequestBody PurchaseEntity purchase){
        purchase.setCreateTime(new Date());
        purchase.setUpdateTime(new Date());
		purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:purchase:update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:purchase:delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
