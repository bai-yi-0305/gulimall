package com.wxs.gulimall.ware.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.constant.WareConstant;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;
import com.wxs.gulimall.ware.dao.PurchaseDao;
import com.wxs.gulimall.ware.entity.PurchaseDetailEntity;
import com.wxs.gulimall.ware.entity.PurchaseEntity;
import com.wxs.gulimall.ware.service.PurchaseDetailService;
import com.wxs.gulimall.ware.service.PurchaseService;
import com.wxs.gulimall.ware.service.WareSkuService;
import com.wxs.gulimall.ware.vo.MergePurchaseVo;
import com.wxs.gulimall.ware.vo.PurchaseDetailVo;
import com.wxs.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageUnReceivePurchase(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
                        //状态[0新建，1已分配，2正在采购，3已完成，4采购失败]
                        .eq("status", 0).or().eq("status", 1)
                        .or().eq("status", 3)
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void mergePurchase(MergePurchaseVo mergePurchaseVo) {
        Long purchaseId = mergePurchaseVo.getPurchaseId();
        //判断是否指定了合并到哪一个采购单，没有指定新建一个采购单
        if (purchaseId == null) {
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATED.getCode());
            purchaseEntity.setPriority(2);
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }
        List<Long> items = mergePurchaseVo.getItems();
        //根据采购项批量查询其具体信息
        List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailService.getBaseMapper().selectBatchIds(items);
        Long finalPurchaseId = purchaseId;
        List<PurchaseDetailEntity> newPurchaseDetail = purchaseDetailEntities.stream().filter(
                purchaseDetailEntity -> {
                    //判断要合并采购项的状态。 只能合并新建或者采购失败（采购失败应该可以在分配/(ㄒoㄒ)/~~猜的）的采购项，
                    // 已分配、正在采购、已完成的采购项不能合并
                    return purchaseDetailEntity.getStatus() == WareConstant.PurchaseDetailStatusEnum.CREATED.getCode()
                            ||
                            purchaseDetailEntity.getStatus() == WareConstant.PurchaseDetailStatusEnum.ERRORED.getCode();
                }
        ).map(purchaseDetailEntity -> {
            purchaseDetailEntity.setPurchaseId(finalPurchaseId);
            purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode());
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        //过滤、设置之后，判断采购项是否为空
        if (!CollectionUtils.isEmpty(newPurchaseDetail)) {
            purchaseDetailService.updateBatchById(newPurchaseDetail);
            //修改更新采购单时间
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setId(finalPurchaseId);
            purchaseEntity.setUpdateTime(new Date());
            this.updateById(purchaseEntity);
        }
    }

    @Transactional
    @Override
    public void received(Long[] purchaseIds) {
        List<Long> purchaseList = Arrays.asList(purchaseIds);
        if (!StringUtils.isEmpty(purchaseList)) {
            //根据采购单id查询当前采购单，并且采购单是已分配状态
            List<PurchaseEntity> purchaseEntityList = this.list(new QueryWrapper<PurchaseEntity>()
                    .in("id", purchaseList)
                    .eq("status", WareConstant.PurchaseStatusEnum.ASSIGNED.getCode()));
            List<PurchaseEntity> updatedPurchaseEntityList = purchaseEntityList.stream().map(purchaseEntity -> {
                //设置采购单状态位已领取
                purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.RECEIVED.getCode());
                purchaseEntity.setUpdateTime(new Date());
                return purchaseEntity;
            }).collect(Collectors.toList());
            //批量更新
            this.updateBatchById(updatedPurchaseEntityList);
            //改变采购项的状态为正在采购
            List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailService.getBaseMapper().selectList(new QueryWrapper<PurchaseDetailEntity>()
                    .in("purchase_id", purchaseIds).eq("status", WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode()));
            if (!StringUtils.isEmpty(purchaseDetailEntities)) {
                List<PurchaseDetailEntity> updatedPurchaseDetailList = purchaseDetailEntities.stream().map(purchaseDetailEntity -> {
                    purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.PURCHASE.getCode());
                    return purchaseDetailEntity;
                }).collect(Collectors.toList());
                purchaseDetailService.updateBatchById(updatedPurchaseDetailList);
            }
        }
    }

    @Transactional
    @Override
    public void done(PurchaseDoneVo purchaseDoneVo) {
        //设置采购项状态
        List<PurchaseDetailVo> items = purchaseDoneVo.getItems();
        List<PurchaseDetailEntity> updatePurchaseDetail = new ArrayList<>();
        //标志位，用来判断是否有没有采购完成的
        boolean flag = true;
        for (PurchaseDetailVo purchaseDetail : items) {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(purchaseDetail.getItemId());
            if (purchaseDetail.getStatus() == WareConstant.PurchaseDetailStatusEnum.ERRORED.getCode()) {
                purchaseDetailEntity.setStatus(purchaseDetail.getStatus());
                flag = false;//有错，没有全部采购完成
            } else {
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FINISHED.getCode());
                //更新库存
                PurchaseDetailEntity detailEntity = purchaseDetailService.getById(purchaseDetailEntity.getId());
                wareSkuService.updateWareSku(detailEntity.getSkuId(),detailEntity.getSkuNum(),detailEntity.getWareId());
            }
            updatePurchaseDetail.add(purchaseDetailEntity);
        }
        purchaseDetailService.updateBatchById(updatePurchaseDetail);
        //设置采购单状态(采购单状态根据采购项是否全部完成设置，全部完成设置已完成，有一个没有完成设为有异常)
        Long purchaseId = purchaseDoneVo.getId();
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());
        if (flag) {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.FINISHED.getCode());
        } else {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.ERRORED.getCode());
        }
        this.updateById(purchaseEntity);
    }
}