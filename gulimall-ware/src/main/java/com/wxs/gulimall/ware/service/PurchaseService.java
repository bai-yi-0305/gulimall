package com.wxs.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxs.common.utils.PageUtils;
import com.wxs.gulimall.ware.entity.PurchaseEntity;
import com.wxs.gulimall.ware.vo.MergePurchaseVo;
import com.wxs.gulimall.ware.vo.PurchaseDoneVo;

import java.util.Map;

/**
 * 采购信息
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:28:47
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnReceivePurchase(Map<String, Object> params);

    void mergePurchase(MergePurchaseVo mergePurchaseVo);

    void received(Long[] purchaseIds);

    void done(PurchaseDoneVo purchaseDoneVo);
}

