package com.wxs.gulimall.ware.dao;

import com.wxs.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:28:47
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
