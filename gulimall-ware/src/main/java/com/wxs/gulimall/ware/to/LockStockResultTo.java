package com.wxs.gulimall.ware.to;

import lombok.Data;

/**
 * @author:Adolph
 * @create:2022/6/21
 */
@Data
public class LockStockResultTo {

    private Long skuId;
    private Integer num;
    private Boolean locked;
}
