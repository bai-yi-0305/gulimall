package com.wxs.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxs.common.constant.OrderConstant;
import com.wxs.common.exception.NoStockException;
import com.wxs.common.to.SkuHasStockTo;
import com.wxs.common.to.mq.OrderTo;
import com.wxs.common.to.mq.StockDetailTo;
import com.wxs.common.to.mq.StockLockedTo;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.Query;
import com.wxs.common.utils.R;
import com.wxs.gulimall.ware.dao.WareSkuDao;
import com.wxs.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.wxs.gulimall.ware.entity.WareOrderTaskEntity;
import com.wxs.gulimall.ware.entity.WareSkuEntity;
import com.wxs.gulimall.ware.feign.OrderFeignService;
import com.wxs.gulimall.ware.feign.ProductFeignService;
import com.wxs.gulimall.ware.service.WareOrderTaskDetailService;
import com.wxs.gulimall.ware.service.WareOrderTaskService;
import com.wxs.gulimall.ware.service.WareSkuService;
import com.wxs.gulimall.ware.to.OrderItemTo;
import com.wxs.gulimall.ware.to.WareSkuLockTo;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private WareSkuDao wareSkuDao;
    @Autowired
    private ProductFeignService productFeignService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private WareOrderTaskDetailService wareOrderTaskDetailService;
    @Autowired
    private WareOrderTaskService wareOrderTaskService;
    @Autowired
    private OrderFeignService orderFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                new QueryWrapper<WareSkuEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> wrapper = new QueryWrapper<>();
        String wareId = (String) params.get("wareId");
        if (!StringUtils.isEmpty(wareId)) {
            wrapper.eq("ware_id", wareId);
        }
        String skuId = (String) params.get("skuId");
        if (!StringUtils.isEmpty(skuId)) {
            wrapper.eq("sku_id", skuId);
        }
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    @Override
    public void updateWareSku(Long skuId, Integer skuNum, Long wareId) {
        //判断是否有这个sku存在，要是第一次操作，就是新增
        WareSkuEntity wareSkuEntity = wareSkuDao.selectOne(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));
        if (wareSkuEntity != null) {
            //更新
            wareSkuDao.updateWareSku(skuId, skuNum, wareId);
        } else {
            //新增
            WareSkuEntity wareSku = new WareSkuEntity();
            wareSku.setSkuId(skuId);
            wareSku.setWareId(wareId);
            wareSku.setStock(skuNum);
            wareSku.setStockLocked(0);
            //TODO 远程查询sku_name.失败无需回滚，
            //自己捕捉
            //2.TODO 还可以用什么办法让异常出现不会滚呢？高级篇！！
            try {
                R info = productFeignService.selectSkuName(skuId);
                Map<String, Object> skuInfo = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0) {
                    wareSku.setSkuName((String) skuInfo.get("skuName"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            wareSkuDao.insert(wareSku);
        }
    }

    @Override
    public List<SkuHasStockTo> getSkuHasStock(List<Long> skuIds) {
        List<SkuHasStockTo> hasStockTos = skuIds.stream().map(skuId -> {
            SkuHasStockTo skuHasStockTo = new SkuHasStockTo();
            //查询当前sku库存
            long stockCount = baseMapper.getHasStock(skuId);
            skuHasStockTo.setSkuId(skuId);
            skuHasStockTo.setHasStock(stockCount > 0);
            return skuHasStockTo;
        }).collect(Collectors.toList());
        return hasStockTos;
    }

    /**
     * 库存解锁的场景
     * 1、下单成功，订单过期未支付，被系统自动取消，被用户手动取消，都要解锁
     * 2、下订单成功，库存锁定成功，接下来的业务调用失败，导致订单回滚，之前锁定的库存就要自动解锁
     *
     * @param to
     * @return
     */
    @Transactional(rollbackFor = {NoStockException.class})
    @Override
    public Boolean orderLockStock(WareSkuLockTo to) {
        /**
         * 保存库存工作单
         */
        WareOrderTaskEntity wareOrderTaskEntity = new WareOrderTaskEntity();
        wareOrderTaskEntity.setOrderSn(to.getOrderSn());
        wareOrderTaskService.save(wareOrderTaskEntity);

        //1、按照下单的收货地址，找到一个就近的仓库，锁定库存
        //找到每一个商品在哪一个仓库有库存
        List<OrderItemTo> locks = to.getLocks();
        List<SkuWareHasStock> skuHasStockWares = locks.stream().map(item -> {
            SkuWareHasStock skuWareHasStock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            skuWareHasStock.setSkuId(skuId);
            //查询这个商品在哪里有库存
            List<Long> wareIds = wareSkuDao.listWareIdHasSkuStock(skuId);
            skuWareHasStock.setWareId(wareIds);
            skuWareHasStock.setNum(item.getCount());
            return skuWareHasStock;
        }).collect(Collectors.toList());
        //2、锁定库存
        for (SkuWareHasStock skuHasStockWare : skuHasStockWares) {
            boolean skuStocked = false;
            Long skuId = skuHasStockWare.getSkuId();
            Integer num = skuHasStockWare.getNum();
            List<Long> wareIds = skuHasStockWare.getWareId();
            if (CollectionUtils.isEmpty(wareIds)) {
                //无库存
                throw new NoStockException(skuId);
            }
            /**
             * 1、如果每一个商品都锁定成功，将当前商品锁定了几件的工作单记录发送给mq
             * 2、如果某个商品锁定失败，前面保存的工作单信息都会回滚
             *      发送出去的消息，即使要解锁记录，由于去数据库查不到该id数据（回滚了），所以就不用解锁
             */
            for (Long wareId : wareIds) {
                Long count = wareSkuDao.lockSkuStock(skuId, wareId, num);
                if (count == 1) {
                    //成功
                    skuStocked = true;
                    //锁定成功，保存工作单详细信息
                    WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity();
                    wareOrderTaskDetailEntity.setSkuId(skuId);
                    wareOrderTaskDetailEntity.setSkuNum(num);
                    wareOrderTaskDetailEntity.setTaskId(wareOrderTaskEntity.getId());
                    wareOrderTaskDetailEntity.setWareId(wareId);
                    wareOrderTaskDetailEntity.setLockStatus(1);
                    wareOrderTaskDetailService.save(wareOrderTaskDetailEntity);
                    //发送消息，告诉mq库存锁定成功
                    StockLockedTo stockLockedTo = new StockLockedTo();
                    stockLockedTo.setId(wareOrderTaskEntity.getId());
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(wareOrderTaskDetailEntity, stockDetailTo);
                    //只发送id不行，防止事务回滚，查不到订单详细数据
                    stockLockedTo.setDetailTo(stockDetailTo);
                    rabbitTemplate.convertAndSend(
                            "stock-event-exchange",
                            "stock.lock",
                            stockLockedTo);
                    break;
                } else {
                    //当前仓库锁失败，重试下一个仓库
                }
            }
            if (!skuStocked) {
                //当前商品所有仓库都没有货，未锁住
                throw new NoStockException(skuId);
            }
        }
        return true;
    }

    /**
     * 库存自动解锁
     * 1、下订单成功，库存锁定成功，接下来的业务调用失败，导致订单回滚，之前锁定的库存就要自动解锁
     * 2、订单失败，锁库存失败
     * <p>
     * 只要解锁库存的消息失败，一定告诉服务器不要删除该消息，开启手动ack机制
     */
    @Override
    public void unLockStock(StockLockedTo to) {
        StockDetailTo detailTo = to.getDetailTo();
        Long detailId = detailTo.getId();
        /**
         * 解锁：
         *  查询数据库关于这个订单的锁定库存信息
         *      有：库存锁定成功，
         *          查看订单情况：
         *          - 订单出现异常，没有创建成功，必须解锁。没成功不是因为库存锁定问题，是因为其它业务调用出现问题，但是库存锁定，没问题，锁定了库存
         *          - 有订单。不是解锁库存
         *              - 订单状态：-已取消：解锁
         *                         -没取消：不能解锁
         *      没有：库存锁定失败，库存回滚了，这种情况无需解锁
         *
         */
        WareOrderTaskDetailEntity taskEntity = wareOrderTaskDetailService.getById(detailId);
        //订单详情存在同时订单详情状态未解锁才解锁
        if (taskEntity != null && taskEntity.getLockStatus() == 1) {
            //解锁
            Long id = to.getId();
            WareOrderTaskEntity orderTaskEntity = wareOrderTaskService.getById(id);
            String orderSn = orderTaskEntity.getOrderSn();
            //根据订单号远程查询订单状态
            R r = orderFeignService.getOrderStatus(orderSn);
            if (r.getCode() == 0) {
                //订单数据查询成功
                OrderTo orderTo = r.getData(new TypeReference<OrderTo>() {
                });
                if (orderTo == null || orderTo.getStatus() == OrderConstant.OrderConstantEnum.CANCELLED.getCode()) {
                    //订单不存在 或者 订单取消，解锁库存
                    unLockStocked(detailTo);
                }
            } else {
                //消息拒绝以后重新放入队列，让别人继续消费解锁
                throw new RuntimeException("远程服务失败");
            }
        }
    }

    @Transactional
    @Override
    public void unLockStock(OrderTo orderTo) {
        String orderSn = orderTo.getOrderSn();
        //查询最新库存的状态，防止重读解锁
        WareOrderTaskEntity wareOrderTaskEntity = wareOrderTaskService.getOrderTaskByOrderSn(orderSn);
        Long id = wareOrderTaskEntity.getId();
        //按照工作单找到所有，没有解锁的库存，进行解锁
        List<WareOrderTaskDetailEntity> orderTaskDetailEntityList = wareOrderTaskDetailService
                .list(new QueryWrapper<WareOrderTaskDetailEntity>()
                        .eq("task_id", id).eq("lock_status", 1));
        for (WareOrderTaskDetailEntity wareOrderTaskDetailEntity : orderTaskDetailEntityList) {
            StockDetailTo stockDetailTo = new StockDetailTo();
            BeanUtils.copyProperties(wareOrderTaskDetailEntity, stockDetailTo);
            unLockStocked(stockDetailTo);
        }
    }

    private void unLockStocked(StockDetailTo detailTo) {
        wareSkuDao.unLockStock(detailTo.getSkuId(), detailTo.getWareId(), detailTo.getSkuNum());
        WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity();
        wareOrderTaskDetailEntity.setId(detailTo.getId());
        wareOrderTaskDetailEntity.setLockStatus(2);
        wareOrderTaskDetailService.getBaseMapper().updateById(wareOrderTaskDetailEntity);
    }

    @Data
    static class SkuWareHasStock {
        private Long skuId;
        private Integer num;
        private List<Long> wareId;
    }
}
