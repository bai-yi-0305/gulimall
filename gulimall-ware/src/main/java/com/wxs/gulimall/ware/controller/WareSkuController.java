package com.wxs.gulimall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.wxs.common.exception.NoStockException;
import com.wxs.common.to.SkuHasStockTo;
import com.wxs.gulimall.ware.to.WareSkuLockTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wxs.gulimall.ware.entity.WareSkuEntity;
import com.wxs.gulimall.ware.service.WareSkuService;
import com.wxs.common.utils.PageUtils;
import com.wxs.common.utils.R;



/**
 * 商品库存
 *
 * @author adolph
 * @email ${email}
 * @date 2022-05-11 19:28:47
 */
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 库存锁定
     * @param to
     * @return
     */
    @PostMapping("/lock/order")
    public R orderLockStock(@RequestBody WareSkuLockTo to){
        try {
            Boolean isLocked = wareSkuService.orderLockStock(to);
            return R.ok().put("isLocked",isLocked);
        } catch (NoStockException e) {
            return R.error(21000,"商品库存不足");
        }
    }

    //查询sku是否有库存
    @PostMapping("/hasStock")
    public R getSkuHasStock(@RequestBody List<Long> skuIds){
        List<SkuHasStockTo> skuHasStockTos = wareSkuService.getSkuHasStock(skuIds);
        return R.ok().setData(skuHasStockTos);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:waresku:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:waresku:info")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:waresku:save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:waresku:update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:waresku:delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
