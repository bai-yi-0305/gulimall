package com.wxs.gulimall.thirdpart.component;

import com.wxs.common.exception.BizCodeEnum;
import com.wxs.common.utils.R;
import com.wxs.gulimall.thirdpart.utils.HttpUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author:Adolph
 * @create:2022/6/4
 * 短信服务
 */
@Data
@Slf4j
@Component
@ConfigurationProperties(prefix = "spring.cloud.alicloud.message")
public class SmsComponent {

    private String host;
    private String path;
    private String method;
    private String appcode;
    private String templateId;

    public R sendMessage(String phone, String code){
        Map<String, String> headers = new HashMap<>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<>();
        Map<String, String> bodys = new HashMap<>();
        bodys.put("content", "code:" + code);
        bodys.put("phone_number", phone);
        bodys.put("template_id", templateId);

        HttpResponse response = null;
        try {
            response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            log.info("response状态信息：{}",response.toString());
            log.info("获取response的body:{}",EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response != null && response.getStatusLine().getStatusCode() == 200){
                return R.ok();
        }else {
            return R.error(BizCodeEnum.SEND_SMS_CODE_ERROR.getCode(), BizCodeEnum.SEND_SMS_CODE_ERROR.getMsg());
        }
    }
}
