package com.wxs.gulimall.thirdpart.controller;

import com.wxs.common.utils.R;
import com.wxs.common.vo.SmsVo;
import com.wxs.gulimall.thirdpart.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:Adolph
 * @create:2022/6/4
 */
@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsComponent smsComponent;

    /**
     * 提供给别的服务调用
     * @return
     */
    @PostMapping("/sendCode")
    public R sendCode(@RequestBody SmsVo smsVo){
        return smsComponent.sendMessage(smsVo.getPhone(),smsVo.getCode());
    }
}
